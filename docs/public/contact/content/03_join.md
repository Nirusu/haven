---
imageLink: contact/content/hours.svg
imageAlt: Haven Office Hours
link1Text:
link1Href:
link2Text:
link2Href:
---

## Office Hours

Iedere woensdag tussen 10.00 en 11.00 is er een inloopuurtje via [Jitsi](https://meet.jit.si/Haven-Office-Hours) voor gemeenten en leveranciers. Iedereen is van harte welkom om vragen te stellen en bij te praten.
