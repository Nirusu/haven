---
title: Hoe werkt het?
introText: Haven legt de verbinding tussen ontwikkelde applicaties en de IT infrastructuur die gemeenten gebruiken. Hiermee kunnen de applicaties overal gehost worden zonder ze aan te moeten passen aan de specifieke infrastructuur waarop deze moeten draaien.
# If no value given, buttons won't render
button1Text: Technische documentatie
button1Link: /techniek
button2Text:
button2Link:
# Edit diagram text in SVG files
diagramA11yText: Haven legt de verbinding tussen ontwikkelde applicaties en de IT infrastructuur die gemeenten gebruiken.
diagramFeaturesLinkHref: /features
---

Meer weten over de werking van Haven?
