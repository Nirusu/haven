## 4. Contact

Mocht u nog vragen hebben, die niet in dit document, noch in het document met algemene informatie, worden beantwoord, of als u om een andere reden contact wil opnemen met de gemeente in het kader van privacy of gegevensbescherming, neem dan contact op via
[realisatie@vng.nl](mailto:realisatie@vng.nl).
