---
title: Privacyverklaring
subtitle: Privacy & security wordt integraal meegenomen in het ontwerp van onze applicaties. Het is een van de kernprincipes van Common Ground.
---

Deze privacyverklaring moet worden gelezen in aanvulling op en in samenhang met <a target="_blank" rel="noopener" href="https://vng.nl/privacyverklaring-vereniging-van-nederlandse-gemeenten">de algemene privacyverklaring van de Vereniging van Nederlandse Gemeenten</a>.
