## 1. Algemeen

VNG Realisatie BV verwerkt persoonsgegevens op een zorgvuldige en veilige manier in overeenstemming met de geldende wet- en regelgeving. In de algemene privacyverklaring zijn de uitgangspunten beschreven ten
aanzien van de verwerking van persoonsgegevens door de VNG Realisatie BV en is informatie te vinden over hoe u uw rechten kunt uitoefenen. In deze meer specifieke verklaring geven we nadere informatie over de doelen en grondslagen, alsmede andere belangrijke informatie.

Vanwege nieuwe wetgeving of andere ontwikkelingen, past VNG Realisatie BV regelmatig haar processen aan. Dit kan ook wijzigingen inhouden ten aanzien van het verwerken van persoonsgegevens. Het is daarom raadzaam om regelmatig de algemene en deze specifieke privacyverklaring te raadplegen. Aan het einde van de verklaring staat aangegeven wanneer deze voor het laatst is aangepast.
