## 5. Uw rechten uitoefenen en klachten

Als burger of belanghebbende kunt u uw rechten bij de VNG Realisatie BV uitoefenen. Voor meer informatie over het uitoefenen van uw rechten, kunt u contact met ons opnemen via [realisatie@vng.nl](mailto:realisatie@vng.nl). Wanneer u een klacht heeft over het handelen van de VNG Realisatie BV, dan heeft u het recht om klacht in te dienen bij de privacy toezichthouder, de Autoriteit Persoonsgegevens.
