---
title: "Inkoop"
path: "/inkoop"
---

Gemeenten kunnen ervoor kiezen om installatie en beheer van Haven in te kopen. Om het inkoop proces makkelijker te maken hebben we een handreiking programma van eisen document gepubliceerd op deze website.

## Disclaimer

U kunt na de nodige aanpassingen te hebben gedaan dit document invoegen in een aanvraag evenals bijvoorbeeld algemene voorwaarden. Let op: er kunnen geen rechten worden ontleend aan deze handreiking. Het is van belang dat u inhoudelijk het document doorneemt en waar nodig aanpast.

## Download

[Handreiking programma van eisen Haven.](/technique/content/documentation/eenvoudig-beheer/inkoop/Handreiking-PvE-Haven-20220818.docx)
