# Build docs.
FROM        node:16.16.0-alpine AS build

# - Copy package.json & package-lock.json to make the dependency fetching step optional.
COPY        ./docs/package.json ./docs/package-lock.json ./docs/.babelrc.js ./docs/jsconfig.json /app/

# - Contents.
COPY        ./haven/cli/pkg/compliancy/static/checks.yaml /haven/cli/pkg/compliancy/static/
COPY        ./docs/pages /app/pages/
COPY        ./docs/public /app/public/
COPY        ./docs/src /app/src/

WORKDIR     /app

# - Build.
RUN         npm install && npm run build

# Create version.json.
FROM        alpine:3.16.1 AS version

RUN         apk add --update jq

ARG         HAVEN_VERSION=undefined
RUN         jq -ncM --arg version $HAVEN_VERSION '{version: $version}' | tee /version.json

# Copy static docs to alpine-based nginx container.
FROM        nginx:1.23.1-alpine
EXPOSE      8080

COPY        ./docs/docker/nginx.conf /etc/nginx/nginx.conf
COPY        ./docs/docker/default.conf /etc/nginx/conf.d/default.conf

COPY        --from=build /app/out /usr/share/nginx/html
COPY        --from=version /version.json /usr/share/nginx/html

RUN         chown -R nginx /usr/share/nginx/html
