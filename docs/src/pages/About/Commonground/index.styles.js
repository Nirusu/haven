// Copyright © VNG Realisatie 2019-2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import BaseSection from 'src/components/Section'

export const Section = styled(BaseSection)`
  padding-top: 0;
`

export const LinkWrapper = styled.div`
  margin: ${(p) => p.theme.tokens.spacing06} 0;
`
