#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

output "credentials" {
  value = <<EOF

  To authenticate to the cluster, please run:

  gcloud container clusters get-credentials ${module.gke.cluster_name} --region=${var.region}

  Then run haven:

  haven check

  EOF
}

output "cluster_name" {
  value = module.gke.cluster_name
}

output "cluster_region" {
  value = module.gke.cluster_region
}

output "network_name" {
  value = module.network.network_name
}

output "nfs_host" {
  value = module.filestore.filestore_ip
}

output "nfs_share_name" {
  value = var.nfs_share_name
}
