# VPC networking

Sets up VPC networking in the project.

<!-- BEGIN TFDOC -->
## Variables

| name | description | type | required | default |
|---|---|:---: |:---:|:---:|
| cidr_range | Subnet CIDR | <code title="">string</code> | ✓ |  |
| cidr_range_ilb | L7 ILB CIDR | <code title="">string</code> | ✓ |  |
| cidr_range_pods | GKE pod CIDR | <code title="">string</code> | ✓ |  |
| cidr_range_services | GKE services CIDR | <code title="">string</code> | ✓ |  |
| network | VPC name | <code title="">string</code> | ✓ |  |
| project_id | GCP project ID | <code title="">string</code> | ✓ |  |
| region | Region to deploy VPC into | <code title="">string</code> | ✓ |  |

## Outputs

| name | description | sensitive |
|---|---|:---:|
| gke_pod_subnet | None |  |
| gke_services_subnet | None |  |
| gke_subnet | None |  |
| network_name | None |  |
| subnets | None |  |
<!-- END TFDOC -->
