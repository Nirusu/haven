# Compliance testing

Sets up the cluster with some Haven and Sonobuoy specific settings. In general,
the cluster would be compliant but the compliance testing introduces some
additional configurations and firewall rules.

<!-- BEGIN TFDOC -->
## Variables

| name | description | type | required | default |
|---|---|:---: |:---:|:---:|
| cidr_range_controlplane | GKE control plane range (/28) | <code title="">string</code> | ✓ |  |
| network_name | VPC name | <code title="">string</code> | ✓ |  |
| project_id | GCP project ID | <code title="">string</code> | ✓ |  |
| *access_token* | Access token | <code title="">string</code> |  | <code title=""></code> |
| *admin_email* | Deployment user email or service account email | <code title="">string</code> |  | <code title=""></code> |
| *ca_certificate* | Control panel CA certificate | <code title="">string</code> |  | <code title=""></code> |
| *compliance_testing_use* | Set to true if this cluster is being used for compliance testing | <code title="">bool</code> |  | <code title="">true</code> |
| *kubernetes_endpoint* | Kubernetes endpoint for patching | <code title="">string</code> |  | <code title=""></code> |

## Outputs

<!-- END TFDOC -->
