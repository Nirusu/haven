# Reference Implementation: Haven on Amazon (AWS)

- [1. Prerequisites](#1-prerequisites)
  - [1.1. Worker node encryption](#11-worker-node-encryption)
    - [1.1.1. Encryption via copy](#111-encryption-via-copy)
    - [1.1.2. Enable default encryption](#112-enable-default-encryption)
- [2. About the solution](#2-about-the-solution)
  - [2.1. Code](#21-code)
  - [2.2. CloudFormation structure](#22-cloudformation-structure)
- [3. Configuration](#3-configuration)
  - [3.1. Region](#31-region)
  - [3.2. API Type](#32-api-type)
  - [3.3. API access CIDRs](#33-api-access-cidrs)
  - [3.4. Kubernetes Version](#34-kubernetes-version)
  - [3.5. Cluster node AMI](#35-cluster-node-ami)
- [4. Additional considerations before deployment](#4-additional-considerations-before-deployment)
  - [4.1. VPC](#41-vpc)
  - [4.2. Worker node internet access](#42-worker-node-internet-access)
  - [4.3. Worker node OS image](#43-worker-node-os-image)
  - [4.4. Restricting access to the instance profile assigned to the worker node](#44-restricting-access-to-the-instance-profile-assigned-to-the-worker-node)
  - [4.5. Additional documentation](#45-additional-documentation)
- [5. Deploying](#5-deploying)
  - [5.1. Bootstrap the aws account](#51-bootstrap-the-aws-account)
  - [5.2. Run the deployment](#52-run-the-deployment)
  - [5.3. Post deployment steps (optional)](#53-post-deployment-steps-optional)
    - [5.3.1. Autoscaler](#531-autoscaler)
    - [5.3.2. Calico](#532-calico)
    - [5.3.3. Hello world](#533-hello-world)
    - [5.3.4. Check for Kubernetes updates](#534-check-for-kubernetes-updates)
- [6. Haven](#6-haven)
  - [6.1. Running](#61-running)
  - [6.2. Results](#62-results)

This package creates a Virtual Private Cloud (VPC), an Elastic File System (EFS) and an Elastic Kubernetes Service (EKS) Cluster.

Summary of resources that are created:
* A new VPC with public and private subnets in up to 3 Availability Zones (AZ)
* NAT Gateways in the public subnets to allow egress to the internet
* An EFS volume with an appropriate Security Group that allows access to the EKS cluster
* An EKS cluster spanning all private subnets
  * The cluster is configured to run Kubernetes 1.21
  * The worker nodes run `Ubuntu for EKS`
  * Logging to CloudWatch using the `fluentd` client
  * Metrics Server
  * Certificate Manager `cert-manager` for automated certificate creation
* The deployment is facilitated by CloudFormation.

![Architecture Diagram](docs/haven-eks.png)

## 1. Prerequisites

This package requires `node` v10.13.0 or later to be installed ([see CDK getting started guide](https://docs.aws.amazon.com/cdk/v2/guide/getting_started.html)). We recommend the latest LTS, which at the time of writing is **v16**.

* [Installing Node](https://nodejs.org/en/download/) see the instructions at the bottom of the page.

The the Haven Compliancy Checker doesn't require `kubectl` but installing it is recommended as it allows you to inspect and validate the cluster status. See [Install K8s tools](https://kubernetes.io/docs/tasks/tools/).

The `aws cli` must be installed and registered in `PATH` (see [Installing the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)). Make sure that the CLI is configured for the AWS account that the solution will be deployed to (see [Configuring the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)).

You can verify that the CLI points to the correct account by running:
```bash
aws sts get-caller-identity --query "Account" --output text
```

### 1.1. Worker node encryption
This step is **optional** but highly recommended!

This solution uses Ubuntu for EKS for the EKS worker nodes. The Amazon Machine Images (AMIs) are not encrypted at rest by default. We strongly recommend encryption at rest for the worker node volumes.

#### 1.1.1. Encryption via copy

To achieve EBS volume encryption, we can create a copy of the relevant AMI into our own AWS account and encrypt the root volume snapshot so that any instance created from that snapshot will be encrypted too.

To create an AMI from Ubuntu for EKS with encryption at rest, follow these steps:

1. Find the relevant AMI ID from the [Canonical Ubuntu for EKS page](https://cloud-images.ubuntu.com/docs/aws/eks/). Select the correct Kubernetes version (1.21 in our case) and find the AMD64 ID for your deployment region. Make a note of this ID (it will start with `ami-`)
2. In your AWS Console, go to the EC2 service and navigate to *AMIs*.
3. Switch the AMI table to *Public Images*.
4. Set the filter to `AMI ID = <your Ubuntu AMI id>`
5. Select the AMI and go to *Actions* > *Copy AMI*
6. In the copy wizard, make sure to select "Encrypt EBS snapshots of AMI copy". You can configure a KMS key of your own or leave the default of `aws/ebs`
7. Make a note of the AMI ID of your copied AMI.
8. Switch the AMI table back to *Owned by me* and monitor the progress of the copy operation. This may take several minutes.
9. When the copy is complete, configure the AMI ID of your copy as the `clusterNodeAmi` in the config of this solution.

#### 1.1.2. Enable default encryption

An easier way to achieve EBS encryption is to enable enable default EBS encryption in the EC2 EBS settings:
1. Open the [EC2 EBS settings](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Settings:tab=ebsEncryption) in your AWS console. **NOTE:** *This is a regional setting, verify that your are in the region that your EKS cluster will be deployed to!*.
2. Click "Manage" and specify the KMS key that is to be used for EBS volume encryption
3. Save your changes by clicking "Update EBS encryption"

You can now reference the original AMI from the [Canonical Ubuntu for EKS page](https://cloud-images.ubuntu.com/docs/aws/eks/) in the `clusterNodeAmi` configuration of this solution. All EBS volumes of your worker nodes will be automatically encrypted using the KMS key you have specified.

## 2. About the solution

### 2.1. Code
The package is using the following technologies
* [TypeScript](https://www.typescriptlang.org/): A modern, type-safe language.
* [CDK](https://docs.aws.amazon.com/cdk/v2/guide/home.html): The AWS CDK lets you build reliable, scalable, cost-effective applications in the cloud with the considerable expressive power of a programming language.
* [CDK8s](https://cdk8s.io/): An open-source software development framework for defining Kubernetes applications and reusable abstractions using familiar programming languages and rich object-oriented APIs.

The code is validated by [cdk-nag](https://github.com/cdklabs/cdk-nag) which "*Checks CDK applications for best practices using a combination of available rule packs*". We have applied the *AWS Solutions* rule pack. Additional rule packs can be applied, for example the **HIPAA** or **PCI DSS** compliance packs.

### 2.2. CloudFormation structure
The solution will install three CloudFormation stacks in your account:
* `HavenInfraStack` - Contains underlying infrastructure
  * A Virtual Private Cloud (VPC) to host the cluster. The VPC has flow logging enabled and will log network traffic to CloudWatch
  * An Elastic File System (EFS) to serve as a permanent storage volume.
  * A KMS key for envelope encryption
* `HavenClusterStack` - Contains the cluster
  * The EKS Kubernetes cluster
  * Creates a cluster node group with three EC2 instances from the configured AMI
    * The launch template restricts instance profile access for cluster nodes ([more info](https://aws.github.io/aws-eks-best-practices/security/docs/iam/#restrict-access-to-the-instance-profile-assigned-to-the-worker-node)).
  * Installs the EFS volume and driver to the cluster
  * Installs the [AWS Load Balancer Controller](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/)
  * Deploys a hello world pod
* `HavenAddonStack` - Contains various addons to the cluster
  * Update the aws-node daemonset to use IRSA ([more info](https://aws.github.io/aws-eks-best-practices/security/docs/iam/#update-the-aws-node-daemonset-to-use-irsa)).
  * Installs `cert-manager`.
  * Installs metrics server
  * Installs logging to CloudWatch using `fluentd`

## 3. Configuration

Before we change the configuration, the node packages need to be installed. You only need to do this once.

```bash
npm install
```

All configuration settings can be found in the file `bin/aws-haven.ts`. The following values need to be configured:

### 3.1. Region

Specifies the AWS region to deploy to. A list of available regions can be found [here](https://docs.aws.amazon.com/general/latest/gr/rande.html).

Default: `eu-central-1` (Frankfurt)

### 3.2. API Type

Specifies the security configuration of the EKS cluster API. The options are:
 * `public` - The cluster endpoint is accessible from outside of your VPC. Worker node traffic will leave your VPC to connect to the endpoint.
 * `private` - The cluster endpoint is only accessible through your VPC. Worker node traffic to the endpoint will stay within your VPC.
 * `public_and_private` - The cluster endpoint is accessible from outside of your VPC. Worker node traffic to the endpoint will stay within your VPC.

**Notes:**
 * When choosing `public_and_private`, make sure to also configure
   `eksApiAccessCidrs` to the IPs that you want to allow to access the API.
   You must ensure that the CIDR blocks that you specify include the addresses
   that worker nodes and Fargate pods (if you use them) access the public endpoint from.
 * `private` mode provides the best security, however, to run the Haven Compliancy Checker in private mode, you need to create an EC2 instance in the VPC and run the check inside of that instance.

Default: no default, please choose an option.

### 3.3. API access CIDRs

If you have chosen `public_and_private` in the API type configuration, you need to limit 
the access to your API by configuring an allowed CIDR range or CIDR ranges. We recommend using a CIDR mask of `/32` to allow access to single IPs which represent the machines that are running the Haven Compliancy Checker.

Example: `["1.2.3.4/32", "5.6.7.8.9/32"]`
Default: `["0.0.0.0/32"]` (no access)

### 3.4. Kubernetes Version

Specify the version of Kubernetes. At the time of writing, the default version for EKS is `1.21`.

This project uses [CDK8s](https://cdk8s.io/docs/latest/) to define K8s charts via CDK code. If you change the version of K8s, you also need to change the imported library package `cdk8s-plus-21` accordingly.

Default: `KubernetesVersion.V1_21`

### 3.5. Cluster node AMI

Specifies the Amazon Machine Image Id (AMI) that will be used for creating EC2 worker node instances. See the [section on worker node encryption](#11-worker-node-encryption) on how to get your AMI.

If you wish to use worker nodes without encryption at rest (**not recommended!**) you can specify the AMI from the [canonical website here](https://cloud-images.ubuntu.com/docs/aws/eks/).

Default: no default, needs to be specified.

## 4. Additional considerations before deployment

### 4.1. VPC

This project creates a default Virtual Private Cloud (VPC) in AWS. In a production scenario
you may prefer creating your own VPC following your specific security and
compliance requirements. The following resources may be useful:
* [Amazon Virtual Private Cloud User Guide](https://docs.aws.amazon.com/vpc/latest/userguide/index.html)
* [Security best practices for your VPC](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-security-best-practices.html)

### 4.2. Worker node internet access

The worker nodes of the created EKS cluster are placed in the **private subnets** of your VPC, route tables and NAT gateways are automatically provided and placed into **public subnets**. In other words, your EKS worker nodes can access the internet outbound but inbound connections are not possible. Depending on your requirements, you may wish to prevent your worker nodes from accessing the internet. To do this, the following resources may be useful:

* [Amazon EKS User Guide](https://docs.aws.amazon.com/eks/latest/userguide/index.html)
* [De-mystifying cluster networking for Amazon EKS worker nodes](https://aws.amazon.com/blogs/containers/de-mystifying-cluster-networking-for-amazon-eks-worker-nodes/)
* [Private cluster requirements](https://docs.aws.amazon.com/eks/latest/userguide/private-clusters.html)

### 4.3. Worker node OS image

This reference implementation uses Ubuntu images as worker nodes. This is only an example, other OS images are available. Consider using Flatcar Linux, Project Atomic, RancherOS, and [Bottlerocket](https://github.com/bottlerocket-os/bottlerocket/), a special purpose OS from AWS designed for running Linux containers. It includes a reduced attack surface, a disk image that is verified on boot, and enforced permission boundaries using SELinux.

* [Amazon EKS optimized AMIs](https://docs.aws.amazon.com/eks/latest/userguide/eks-optimized-amis.html)
* [EKS Best Practices Guides - Use an OS optimized for running containers](https://aws.github.io/aws-eks-best-practices/security/docs/hosts/#use-an-os-optimized-for-running-containers)

### 4.4. Restricting access to the instance profile assigned to the worker node

We are restricting access of pods to the instance profile of the worker node to prevent privilege escalations ([more info](https://aws.github.io/aws-eks-best-practices/security/docs/iam/#restrict-access-to-the-instance-profile-assigned-to-the-worker-node)).

However, this means that pods that do not use IRSA are prevented from inheriting the role assigned to the worker node.

### 4.5. Additional documentation

* [Application load balancing on Amazon EKS](https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html)
* [Amazon CloudWatch Guide - Send logs to Cloudwatch Logs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/Container-Insights-EKS-logs.html)
* [EKS Best Practises - Kubernetes Cluster Autoscaler](https://aws.github.io/aws-eks-best-practices/cluster-autoscaling/)
* [cert-manager documentation](https://cert-manager.io/docs/)
* [Kubernetes Metrics Server](https://github.com/kubernetes-sigs/metrics-server)

## 5. Deploying

### 5.1. Bootstrap the aws account
The AWS account needs to be bootstrapped to be used with CDK. This needs to be done only once.
```bash
npm run cdk -- bootstrap
```

The bootstrapping process creates resources that CDK needs to deploy applications. More info can be found [here](https://docs.aws.amazon.com/cdk/v2/guide/bootstrapping.html).

### 5.2. Run the deployment
Deploy the infrastructure:
```bash
npm run deploy
```

There will be prompts to confirm the IAM roles and permissions that are about to be created in your account. Please review the provided summary and agree if you are happy to go ahead.

The deployment may run for 30 minutes or more. When the deployment is complete, you will see output similar to (you may have to scoll up, the output is created by the `HavenClusterStack`):
```
Outputs:
HavenClusterStack.EksClusterConfigCommand2AE6ED67 = aws eks update-kubeconfig --name haven --region eu-central-1 --role-arn arn:aws:iam::123456789012:role/HavenClusterStack-EksClusterMastersRole3F49FA-158ORRYGXTE9H
HavenClusterStack.EksClusterGetTokenCommandDF0BEDB9 = aws eks get-token --cluster-name haven --region eu-central-1 --role-arn arn:aws:iam::123456789012:role/HavenClusterStack-EksClusterMastersRole3F49FA-158ORRYGXTE9H
```

### 5.3. Post deployment steps (optional)
Take the value of the `HavenClusterStack.EksClusterConfigCommand***` output and run it in the current shell. The command will configure `kubectl` to access your Kubernetes cluster. [More information about the CLI command](https://docs.aws.amazon.com/cli/latest/reference/eks/update-kubeconfig.html).

If you have configured the API endpoint to be **private**, you must configure `kubectl` on a device that has access to the VPC, for example an EC2 instance.

#### 5.3.1. Autoscaler

We recommend adding the Kubernetes Cluster Autoscaler to your EKS cluster. To make this easier, we have provided a CDK construct called `ClusterAutoscaler` with this solution.

Refer to the [EKS Best Practices Guide](https://aws.github.io/aws-eks-best-practices/cluster-autoscaling/) for more information.

#### 5.3.2. Calico

Project Calico is a network policy engine for Kubernetes. With Calico network policy enforcement, you can implement network segmentation and tenant isolation. This is useful in multi-tenant environments where you must isolate tenants from each other or when you want to create separate environments for development, staging, and production.

Refer to the [Calico Installation guide](https://docs.aws.amazon.com/eks/latest/userguide/calico.html) for more info.

#### 5.3.3. Hello world

You may wish to install some further pods to verify cluster functionality. We've provided a Hello World chart at `lib/charts/hello-world-pod.chart.ts` which installs a Hello World pod.

See the code below on how to wire this chart into your deployment.

```ts
// lib/stacks/cluster.stack.ts

import { HelloWorldPodChart } from "../charts/hello-world-pod.chart";
// ...

/**
 * The stack that contains the EKS cluster
 */
export class ClusterStack extends Stack {
  constructor(scope: Construct, id: string, props: EksStackProps) {
    // ...

    /**
     * Hello World Pod
     */
    const helloWorldPod = cluster.addCdk8sChart(
      "helloWorld",
      new HelloWorldPodChart(cdk8sApp, "HelloWorldPod", {
        serviceAccountName: eksServiceAccount.serviceAccountName,
      })
    );

    helloWorldPod.node.addDependency(eksServiceAccount);
  }
}
```

This example chart is given as a starting point to develop additional pod deployments using the CDK Infrastructure as Code (IaC) approach. Once the deployment is defined, it can be turned into a source code driven continuous integration and deployment (CI/CD) pipeline using [CDK pipelines](https://docs.aws.amazon.com/cdk/api/v2/docs/aws-cdk-lib.pipelines-readme.html) hosted on AWS.

#### 5.3.4. Check for Kubernetes updates

We recommend to update your EKS cluster to the latest version of Kubernetes. To do this, find your cluster in the [AWS Management console](https://eu-central-1.console.aws.amazon.com/eks/home?region=eu-central-1#/clusters). If a new version is available, there will be an **"Update now"** link next to the cluster. Click on this link and follow the instructions.

## 6. Haven

### 6.1. Running

Instructions on how to run the Haven Compliancy Checker can be found [here](https://haven.commonground.nl/techniek/compliancy-checker).

### 6.2. Results

```
+----------------+--------------------------------------------------------------------+--------+
|    CATEGORY    |                                NAME                                | PASSED |
+----------------+--------------------------------------------------------------------+--------+
| Fundamental    | Self test: does HCC have cluster-admin                             | YES    |
| Infrastructure | Multiple availability zones in use                                 | YES    |
| Infrastructure | Running at least 3 master nodes                                    | YES    |
| Infrastructure | Running at least 3 worker nodes                                    | YES    |
| Infrastructure | Nodes have SELinux, Grsecurity, AppArmor or LKRG enabled           | YES    |
| Infrastructure | Private networking topology                                        | YES    |
| Cluster        | Kubernetes version is latest stable or max 2 minor versions behind | YES    |
| Cluster        | Role Based Access Control is enabled                               | YES    |
| Cluster        | Basic auth is disabled                                             | YES    |
| Cluster        | ReadWriteMany persistent volumes support                           | YES    |
| Cluster        | LoadBalancer service type support                                  | YES    |
| External       | CNCF Kubernetes Conformance                                        | YES    |
| Deployment     | Automated HTTPS certificate provisioning                           | YES    |
| Deployment     | Log aggregation is running                                         | YES    |
| Deployment     | Metrics-server is running                                          | YES    |
+----------------+--------------------------------------------------------------------+--------+

[I] Suggested checks results:

+------------+-----------------------------------+---------+
|  CATEGORY  |               NAME                | PASSED  |
+------------+-----------------------------------+---------+
| Deployment | Haven Dashboard is provisioned    | NO      |
| External   | CIS Kubernetes Security Benchmark | SKIPPED |
+------------+-----------------------------------+---------+
```

The CIS benchmark can be run on EKS too. Please refer to this [guide](https://aws.amazon.com/blogs/containers/introducing-cis-amazon-eks-benchmark/).