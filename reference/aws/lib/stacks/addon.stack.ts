import { Stack, StackProps } from "aws-cdk-lib";
import { ICluster, INodegroup } from "aws-cdk-lib/aws-eks";
import { App } from "cdk8s";
import { Construct } from "constructs";
import { LoggingChart } from "../charts/logging.chart";
import { EnableIrsa } from "../constructs/enable-irsa";
import { MultiManifest } from "../constructs/multi-manifest";

export interface AddonStackProps extends StackProps {
  cluster: ICluster;
  nodegroup: {
    nodegroup: INodegroup;
    nodegroupInternalName: string;
  };
}

export class AddonStack extends Stack {
  constructor(scope: Construct, id: string, props: AddonStackProps) {
    super(scope, id, {
      description: "Contains security and other addons",
      ...props,
    });

    const cdk8sApp = new App();

    this.addDependency(Stack.of(props.cluster));

    /**
     * Update the aws-node daemonset to use IRSA
     *
     * @see https://aws.github.io/aws-eks-best-practices/security/docs/iam/#update-the-aws-node-daemonset-to-use-irsa
     */
    new EnableIrsa(this, "EnableIrsa", {
      cluster: props.cluster,
    });

    /**
     * Install cert-manager
     */
    new MultiManifest(this, "CertManager", {
      cluster: props.cluster,
      manifestPrefix: "cert",
      manifestUrl:
        "https://github.com/cert-manager/cert-manager/releases/download/v1.8.0/cert-manager.yaml",
    });

    /**
     * Metrics Server
     */
    props.cluster.addHelmChart("MetricsServer", {
      chart: "metrics-server",
      release: "metrics-server",
      repository: "https://kubernetes-sigs.github.io/metrics-server/",
      namespace: "kube-system",
    });

    /**
     * CloudWatch logging
     */
    this.installLogging(props.cluster, cdk8sApp);
  }

  private installLogging(cluster: ICluster, cdk8sApp: App) {
    /**
     * Logging namespace and config
     */
    const loggingNamespaceChart = cluster.addCdk8sChart(
      "loggingChart",
      new LoggingChart(cdk8sApp, "LoggingChart", {
        cluster,
      })
    );

    /**
     * FluentD log aggregation
     */
    new MultiManifest(this, "FluentD", {
      cluster,
      dependsOnManifest: loggingNamespaceChart,
      manifestPrefix: "fluentd",
      manifestUrl:
        "https://raw.githubusercontent.com/aws-samples/amazon-cloudwatch-container-insights/latest/k8s-deployment-manifest-templates/deployment-mode/daemonset/container-insights-monitoring/fluentd/fluentd.yaml",
    });
  }
}
