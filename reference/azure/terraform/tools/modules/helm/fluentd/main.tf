resource "kubernetes_namespace" "fluentd" {
  count = var.enable_fluentd ? 1 : 0
  metadata {
    name = var.fluentd_namespace
    labels = {
      deployed-by = "Terraform"
    }
  }
}

resource "helm_release" "fluentd" {
  count      = var.enable_fluentd ? 1 : 0
  name       = "fluentd"
  chart      = "fluent-bit"
  repository = var.fluentd_chart_repository
  version    = var.fluentd_chart_version
  namespace  = kubernetes_namespace.fluentd.0.metadata.0.name

  dynamic "set" {
    for_each = local.fluentd_values
    iterator = setting
    content {
      name  = setting.key
      value = setting.value
    }
  }
}
