BINDIR       := $(CURDIR)/bin
INSTALL_PATH ?= /usr/local/bin
TARGETS      := darwin/amd64 darwin/arm64 linux/amd64 linux/arm64 windows/amd64
BINNAME      ?= haven

GOBIN         = $(shell go env GOBIN)
ifeq ($(GOBIN),)
GOBIN         = $(shell go env GOPATH)/bin
endif
GOX           = $(GOBIN)/gox

LDFLAGS := -w -s
LDFLAGS += -X main.version=${HAVEN_VERSION}

SRC := $(shell find . -type f -name '*.go' -print) go.mod go.sum
VERSION = $(shell git describe --tags)

.PHONY: all
all: build

# ------------------------------------------------------------------------------
#  build
# ------------------------------------------------------------------------------

.PHONY: build
build: $(BINDIR)/$(BINNAME)

$(BINDIR)/$(BINNAME): $(SRC)
	GO111MODULE=on go build $(GOFLAGS) -trimpath -ldflags '$(LDFLAGS)' -o '$(BINDIR)'/$(BINNAME) ./cmd/cli

# ------------------------------------------------------------------------------
#  install
# ------------------------------------------------------------------------------

.PHONY: install
install: build
	@install "$(BINDIR)/$(BINNAME)" "$(INSTALL_PATH)/$(BINNAME)"

# ------------------------------------------------------------------------------
#  release
# ------------------------------------------------------------------------------

$(GOX):
	(cd /; GO111MODULE=on go install github.com/mitchellh/gox@v1.0.1)

.PHONY: build-cross
build-cross: LDFLAGS += -extldflags "-static"
build-cross: $(GOX)
	GOFLAGS="-trimpath" GO111MODULE=on CGO_ENABLED=0 $(GOX) -parallel=3 -output="_dist/{{.OS}}-{{.Arch}}/$(BINNAME)" -osarch='$(TARGETS)' $(GOFLAGS) -ldflags '$(LDFLAGS)' ./cmd/cli

.PHONY: dist
dist:
	( \
		cd _dist && \
		find * -type d -exec zip -r haven-${VERSION}-{}.zip {} \; \
	)

# The contents of the .sha256sum file are compatible with tools like
# shasum. For example, using the following command will verify
# the file haven-v9.0.0-darwin-amd64.tar.gz:
#   shasum -a 256 -c haven-v9.0.0-darwin-amd64.tar.gz.sha256sum
.PHONY: checksum
checksum:
	for f in $$(ls _dist/*.{zip} 2>/dev/null) ; do \
		shasum -a 256 "$${f}" | sed 's/_dist\///' > "$${f}.sha256sum" ; \
	done

.PHONY: info
info:
	@echo "Version:           ${VERSION}"
