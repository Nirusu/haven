# Haven dashboard

This folder should contain a build of the Haven dashboard and will be included in the Haven CLI binary. To manually build the dashboard use:

```bash
(cd community/addons/dashboard && npm install && npm run build)
(cp -R community/addons/dashboard/build/* haven/cli/pkg/dashboard/static/)
```
