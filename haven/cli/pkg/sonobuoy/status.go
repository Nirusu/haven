package sonobuoy

import "encoding/json"

func UnmarshalSonobuoyStatus(data []byte) (SonobuoyStatus, error) {
	var r SonobuoyStatus
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *SonobuoyStatus) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type SonobuoyStatus struct {
	Plugins []Plugin `json:"plugins"`
	Status  string   `json:"status"`
	TarInfo TarInfo  `json:"tar-info"`
}

type Plugin struct {
	Plugin       string      `json:"plugin"`
	Node         string      `json:"node"`
	Status       string      `json:"status"`
	ResultStatus string      `json:"result-status"`
	ResultCounts interface{} `json:"result-counts"`
	Progress     *Progress   `json:"progress,omitempty"`
}

type Progress struct {
	Name      string `json:"name"`
	Node      string `json:"node"`
	Timestamp string `json:"timestamp"`
	Msg       string `json:"msg"`
	Total     int64  `json:"total"`
	Completed int64  `json:"completed"`
}

type TarInfo struct {
	Name    string `json:"name"`
	Created string `json:"created"`
	Sha256  string `json:"sha256"`
	Size    int64  `json:"size"`
}
