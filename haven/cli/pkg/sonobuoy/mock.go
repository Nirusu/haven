package sonobuoy

import (
	"fmt"
)

type MockImpl struct {
	Completed                                    int64
	Total                                        int64
	TestStatus                                   string
	FailRun, FailStatus, FailResults, FailDelete bool
	RetrieveFails, StatusRetrieveFails           int
	RetrieveFailCounter, StatusFailCounter       int
}

const (
	DefaultError string = "this is a sonobuoy error:"
)

func NewMockSonobuoyClient(completed, total int64, statusFails int, failed, failingFunction string) Client {
	if failed == "" {
		failed = "passed"
	}

	var failRun bool
	var failStatus bool
	var failResults bool
	var failDelete bool
	failRetrieve := 1
	failStatusRetrieve := statusFails

	switch failingFunction {
	case "Run":
		failRun = true
	case "Status":
		failStatus = true
	case "Results":
		failResults = true
	case "Delete":
		failDelete = true
	}

	impl := MockImpl{
		Completed:           completed,
		Total:               total,
		TestStatus:          failed,
		FailRun:             failRun,
		FailStatus:          failStatus,
		FailResults:         failResults,
		FailDelete:          failDelete,
		RetrieveFails:       failRetrieve,
		RetrieveFailCounter: 0,
		StatusFailCounter:   0,
		StatusRetrieveFails: failStatusRetrieve,
	}

	return &impl
}

func (m *MockImpl) Run(flags string) error {
	if m.FailRun {
		return fmt.Errorf("%s run %s", DefaultError, flags)
	}
	return nil
}

func (m *MockImpl) Status() (*SonobuoyStatus, error) {
	if m.FailStatus {
		return nil, fmt.Errorf("%s status", DefaultError)
	}

	if m.StatusRetrieveFails != m.StatusFailCounter {
		m.StatusFailCounter++
		return nil, fmt.Errorf("%s status", DefaultError)
	}

	status := "running"
	if m.Completed == m.Total {
		status = "completed"
	}
	sono := SonobuoyStatus{
		Plugins: []Plugin{{
			Plugin:       "e2e",
			Node:         "",
			Status:       status,
			ResultStatus: "",
			ResultCounts: nil,
			Progress: &Progress{
				Name:      "",
				Node:      "",
				Timestamp: "",
				Msg:       "",
				Total:     m.Total,
				Completed: m.Completed,
			},
		},
		},
		Status:  "",
		TarInfo: TarInfo{},
	}

	return &sono, nil
}

func (m *MockImpl) Retrieve() (string, error) {
	if m.RetrieveFails != m.RetrieveFailCounter {
		m.RetrieveFailCounter++
		return "", fmt.Errorf("%s error retrieving results: no valid entries in result", DefaultError)
	}

	return "202201261356_sonobuoy_75554fc0-6cac-490b-b332-eeeaa53f8baa.tar.gz", nil
}

func (m *MockImpl) Results() (string, error) {
	if m.FailResults {
		return "", fmt.Errorf("%s results", DefaultError)
	}
	results := fmt.Sprintf("Plugin: e2e\nStatus: %s\nTotal: 5668\nPassed: 5668\nFailed: 0\nSkipped: 0\n\nPlugin: systemd-logs\nStatus: passed\nTotal: 6\nPassed: 6\nFailed: 0\nSkipped: 0", m.TestStatus)
	return results, nil
}

func (m *MockImpl) DeleteAll() error {
	if m.FailDelete {
		return fmt.Errorf("%s delete", DefaultError)
	}
	return nil
}
