package validator

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestValidOrg(t *testing.T) {
	urls := []string{"commonground", "mijn-organisatie", "a-very-long-org"}

	for _, url := range urls {
		valid := ValidOrganization(url)
		assert.True(t, valid)
	}
}

func TestInvalidOrg(t *testing.T) {
	urls := []string{"dev.commonground.nl", "mijn-organisatie.nl", "http://www.dev.commonground.nl", "l.nl"}

	for _, url := range urls {
		valid := ValidOrganization(url)
		assert.False(t, valid)
	}
}
