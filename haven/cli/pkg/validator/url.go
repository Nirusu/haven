package validator

import (
	"regexp"
)

func ValidUrl(url string) bool {
	regex := "([A-Za-z0-9\\-\\/:]+([.+]))"
	re := regexp.MustCompile(regex)
	match := re.MatchString(url)

	return match
}
