package haven

import (
	"context"
	"fmt"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/models/crds/haven/v1alpha"
	rbacapi "k8s.io/api/rbac/v1"
	apiextensionv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	"k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	extensionsclient "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	k8s "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	rbachelper "k8s.io/kubernetes/pkg/apis/rbac/v1"
	"time"
)

const (
	GroupName              string = "haven.commonground.nl"
	CrdName                string = "compliancies.haven.commonground.nl"
	ClusterRoleName        string = "haven-compliancy-view"
	ClusterRoleBindingName string = "haven-compliancy-view-authenticated"
	Kind                   string = "Compliancy"
	Version                string = "v1alpha1"
	Singular               string = "compliancy"
	ShortName              string = "compliancy"
	Plural                 string = "compliancies"
	Name                          = Plural + "." + GroupName
)

type V1Alpha1 interface {
	List() (*v1alpha.CompliancyList, error)
	Get(name string) (*v1alpha.Compliancy, error)
	Create(compliancy *v1alpha.Compliancy) (*v1alpha.Compliancy, error)
	CreateHavenResource(output string, input v1alpha.Input, compliant bool) (*v1alpha.Compliancy, error)
	CreateHavenDefinition() (bool, error)
	GetDefinition(name string) (*apiextensionv1.CustomResourceDefinition, error)
}

type V1Alpha1Client struct {
	Client          rest.Interface
	ExtensionClient *clientset.Clientset
	RolesClient     kubernetes.Roles
}

func NewCrdConfig(kubeConfig []byte) (V1Alpha1, error) {
	c, err := clientcmd.NewClientConfigFromBytes(kubeConfig)
	if err != nil {
		return nil, err
	}

	restConfig, err := c.ClientConfig()
	if err != nil {
		return nil, err
	}

	config := *restConfig
	config.ContentConfig.GroupVersion = &schema.GroupVersion{Group: v1alpha.GroupName, Version: v1alpha.Version}
	config.APIPath = "/apis"
	config.NegotiatedSerializer = scheme.Codecs.WithoutConversion()
	config.UserAgent = rest.DefaultKubernetesUserAgent()

	client, err := rest.RESTClientFor(&config)
	if err != nil {
		return nil, err
	}

	clientSet, err := k8s.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	extensionClient, err := extensionsclient.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	rolesClient, err := kubernetes.NewRolesClient(clientSet)
	if err != nil {
		return nil, err
	}

	return &V1Alpha1Client{
		Client:          client,
		ExtensionClient: extensionClient,
		RolesClient:     rolesClient,
	}, nil
}

func (c *V1Alpha1Client) CreateHavenResource(output string, input v1alpha.Input, compliant bool) (*v1alpha.Compliancy, error) {
	out, err := v1alpha.UnmarshalOutputResource([]byte(output))
	if err != nil {
		return nil, err
	}

	apiVersion := fmt.Sprintf("%s/%s", v1alpha.GroupName, v1alpha.Version)

	compliancyResource := v1alpha.Compliancy{
		TypeMeta: metav1.TypeMeta{},
		ObjectMeta: metav1.ObjectMeta{
			Name: fmt.Sprintf("haven-%s", time.Now().UTC().Format("20060102-150405")),
		},
		APIVersion: apiVersion,
		Kind:       v1alpha.Kind,
		Spec: v1alpha.Spec{
			Compliant: compliant,
			Created:   time.Now().UTC().String(),
			Version:   fmt.Sprintf("Haven %s", v1alpha.Version),
			Output:    out,
			Input:     input,
		},
	}

	return &compliancyResource, nil
}

func (c *V1Alpha1Client) Create(compliancy *v1alpha.Compliancy) (*v1alpha.Compliancy, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	compl, err := compliancy.Marshal()
	if err != nil {
		return nil, err
	}

	result := v1alpha.Compliancy{}
	err = c.Client.
		Post().
		Resource(v1alpha.Plural).
		Body(compl).
		Do(ctx).
		Into(&result)

	return &result, err
}

func (c *V1Alpha1Client) List() (*v1alpha.CompliancyList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	result := v1alpha.CompliancyList{}
	err := c.Client.
		Get().
		Resource(v1alpha.Plural).
		Do(ctx).
		Into(&result)

	return &result, err
}

func (c *V1Alpha1Client) Get(name string) (*v1alpha.Compliancy, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	result := v1alpha.Compliancy{}
	err := c.Client.
		Get().
		Name(name).
		Resource(v1alpha.Plural).
		Do(ctx).
		Into(&result)

	return &result, err
}

// CreateHavenDefinition retrieves the Haven CRD and will deploy it to the cluster when it's not installed yet.
func (c *V1Alpha1Client) CreateHavenDefinition() (bool, error) {
	var created bool
	crd := v1alpha.CreateDefinition()

	_, err := c.GetDefinition(crd.Name)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			created = true
			_, err := c.ExtensionClient.ApiextensionsV1().CustomResourceDefinitions().Create(context.Background(), crd, metav1.CreateOptions{})
			if err != nil {
				return created, err
			}
		}
	}

	clusterRole := &rbacapi.ClusterRole{
		ObjectMeta: metav1.ObjectMeta{Name: ClusterRoleName},
		Rules: []rbacapi.PolicyRule{
			rbachelper.NewRule("get", "list", "watch").Groups(GroupName).Resources(Plural).RuleOrDie(),
		},
	}

	clusterRoleBinding := &rbacapi.ClusterRoleBinding{
		ObjectMeta: metav1.ObjectMeta{Name: ClusterRoleBindingName},
		RoleRef:    rbacapi.RoleRef{Kind: "ClusterRole", Name: "haven-compliancy-view"},
		Subjects: []rbacapi.Subject{
			{Kind: "Group", Name: "system:authenticated"},
		},
	}

	if err = c.RolesClient.CreateClusterRole(clusterRole); err != nil {
		return created, err
	}

	if err = c.RolesClient.CreateClusterRoleBinding(clusterRoleBinding); err != nil {
		return created, err
	}

	return created, err
}

func (c *V1Alpha1Client) GetDefinition(name string) (*apiextensionv1.CustomResourceDefinition, error) {
	definition, err := c.ExtensionClient.ApiextensionsV1().CustomResourceDefinitions().Get(context.Background(), name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return definition, err
}
