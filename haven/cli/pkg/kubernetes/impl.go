package kubernetes

import (
	"embed"
	"encoding/base64"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	appsv1 "k8s.io/api/apps/v1"
	authorizationv1 "k8s.io/api/authorization/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	v1 "k8s.io/api/storage/v1"
	extv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	extensionsclient "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/version"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

var (
	//go:embed static/data.txt
	embedData embed.FS
)

// KubeClient interface creates a client to interact with k8s
type KubeClient interface {
	Pods() Pod
	Namespaces() Namespace
	Secrets() Secret
	Services() Service
	CustomResourceDefinitions() CustomResourceDefinition
	CustomResources() CustomResource
	Jobs() Job
	VolumeClaims() VolumeClaim
	Storage() Storage
	Nodes() Nodes
	Deployments() Deployment
	AccessReview() AccessReview
	Roles() Roles
	Misc() Misc
}

type AccessReview interface {
	Create(sar *authorizationv1.SelfSubjectAccessReview) (*authorizationv1.SelfSubjectAccessReview, error)
}

type CustomResource interface {
	Delete(group, version, resource, namespace, name string) error
	Get(group, version, resource, namespace, name string) (*unstructured.Unstructured, error)
	List(group, version, resource string) (*unstructured.UnstructuredList, error)
}

type CustomResourceDefinition interface {
	Create(crd *extv1.CustomResourceDefinition) (*extv1.CustomResourceDefinition, error)
	List() (*extv1.CustomResourceDefinitionList, error)
	Delete(name string) error
}

type Deployment interface {
	List(namespace string) (*appsv1.DeploymentList, error)
	Delete(namespace, name string) error
	Create(namespace string, deployment *appsv1.Deployment) (*appsv1.Deployment, error)
}

type Job interface {
	List(namespace string) (*batchv1.JobList, error)
	Delete(namespace, jobName string, pp *metav1.DeletionPropagation) error
	Create(namespace string, job *batchv1.Job) (*batchv1.Job, error)
}

type Misc interface {
	GetServerVersion() (*version.Info, error)
	GetHost() string
	GetServerGroups() (*metav1.APIGroupList, error)
}

type Namespace interface {
	Create(namespace string) error
	Delete(namespace string) error
	List() (*corev1.NamespaceList, error)
}

type Nodes interface {
	List() (*corev1.NodeList, error)
	Create(node *corev1.Node) (*corev1.Node, error)
}

type Pod interface {
	List(namespace string) (*corev1.PodList, error)
	Get(namespace, podName string) (*corev1.Pod, error)
	GetByLabel(namespace, label string) (*corev1.PodList, error)
	Delete(namespace, podName string) error
	Create(namespace string, pod *corev1.Pod) (*corev1.Pod, error)
	Update(namespace string, pod *corev1.Pod) (*corev1.Pod, error)
	ExecNamed(namespace, podName string, command []string) (string, error)
	Logs(namespace, name string) (string, error)
}

type Roles interface {
	CreateClusterRoleBinding(clusterRoleBinding *rbacv1.ClusterRoleBinding) error
	GetClusterRoleBinding(name string) (*rbacv1.ClusterRoleBinding, error)
	CreateClusterRole(clusterRole *rbacv1.ClusterRole) error
	GetClusterRole(name string) (*rbacv1.ClusterRole, error)
}

type Secret interface {
	Create(namespace, secretName, typeAsString string, data map[string][]byte) error
	Get(namespace, secretName string) (*corev1.Secret, error)
	List(namespace string) (*corev1.SecretList, error)
	Delete(namespace, secretName string) error
}

type Service interface {
	Get(namespace, serviceName string) (*corev1.Service, error)
	List(namespace string) (*corev1.ServiceList, error)
	Create(namespace string, svc *corev1.Service) (*corev1.Service, error)
	Delete(namespace, serviceName string) error
}

type Storage interface {
	List() (*v1.StorageClassList, error)
}

type VolumeClaim interface {
	List(namespace string) (*corev1.PersistentVolumeClaimList, error)
	Get(namespace, name string) (*corev1.PersistentVolumeClaim, error)
	Delete(namespace, name string) error
	Create(namespace string, pvc *corev1.PersistentVolumeClaim) (*corev1.PersistentVolumeClaim, error)
}

type Kube struct {
	pod              *PodImpl
	namespace        *NamespaceImpl
	secret           *SecretImpl
	service          *ServiceImpl
	customDefinition *CustomResourceDefinitionImpl
	customResource   *CustomResourceImpl
	job              *JobImpl
	volumeClaim      *PvcImpl
	storage          *StorageImpl
	nodes            *NodeImpl
	deployment       *DeploymentImpl
	accessReview     *AccessReviewImpl
	roles            *RolesImpl
	misc             *MiscImpl

	config     []byte
	restConfig *rest.Config
}

func (k *Kube) Pods() Pod {
	if k == nil {
		return nil
	}

	return k.pod
}

func (k *Kube) Namespaces() Namespace {
	if k == nil {
		return nil
	}

	return k.namespace
}

func (k *Kube) Secrets() Secret {
	if k == nil {
		return nil
	}

	return k.secret
}

func (k *Kube) Services() Service {
	if k == nil {
		return nil
	}

	return k.service
}

func (k *Kube) CustomResourceDefinitions() CustomResourceDefinition {
	if k == nil {
		return nil
	}

	return k.customDefinition
}

func (k *Kube) CustomResources() CustomResource {
	if k == nil {
		return nil
	}

	return k.customResource
}

func (k *Kube) Jobs() Job {
	if k == nil {
		return nil
	}

	return k.job
}

func (k *Kube) VolumeClaims() VolumeClaim {
	if k == nil {
		return nil
	}

	return k.volumeClaim
}

func (k *Kube) Storage() Storage {
	if k == nil {
		return nil
	}

	return k.storage
}

func (k *Kube) Nodes() Nodes {
	if k == nil {
		return nil
	}

	return k.nodes
}

func (k *Kube) Deployments() Deployment {
	if k == nil {
		return nil
	}

	return k.deployment
}

func (k *Kube) AccessReview() AccessReview {
	if k == nil {
		return nil
	}

	return k.accessReview
}

func (k *Kube) Roles() Roles {
	if k == nil {
		return nil
	}

	return k.roles
}

func (k *Kube) Misc() Misc {
	if k == nil {
		return nil
	}

	return k.misc
}

func (k *Kube) KubeCliConfig(namespace string) (*genericclioptions.ConfigFlags, error) {
	kubeConfig := genericclioptions.NewConfigFlags(false)
	kubeConfig.APIServer = &k.restConfig.Host
	kubeConfig.BearerToken = &k.restConfig.BearerToken
	kubeConfig.CAFile = &k.restConfig.CAFile
	kubeConfig.Namespace = &namespace

	return kubeConfig, nil
}

func (k *Kube) GetK8sRestConfig() *rest.Config {
	return k.restConfig
}

func NewClientFromFile(kubeConfig []byte) (KubeClient, error) {
	client, err := New(kubeConfig)
	if err != nil {
		logging.Error("Error getting creating client")
		return nil, err
	}

	return client, nil
}

func New(config []byte) (*Kube, error) {
	c, err := clientcmd.NewClientConfigFromBytes(config)
	if err != nil {
		return nil, err
	}

	restConfig, err := c.ClientConfig()
	if err != nil {
		return nil, err
	}

	clientSet, err := kubernetes.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	extensionsClient, err := extensionsclient.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	podClient, err := NewPodsClient(clientSet, config)
	if err != nil {
		return nil, err
	}

	namespaceClient, err := NewNamespaceClient(clientSet)
	if err != nil {
		return nil, err
	}

	secretClient, err := NewSecretClient(clientSet)
	if err != nil {
		return nil, err
	}

	serviceClient, err := NewServiceClient(clientSet)
	if err != nil {
		return nil, err
	}

	defintionClient, err := NewCRDClient(extensionsClient)
	if err != nil {
		return nil, err
	}

	resourceClient, err := NewCRClient(restConfig)
	if err != nil {
		return nil, err
	}

	jobClient, err := NewJobClient(clientSet)
	if err != nil {
		return nil, err
	}

	volumeClient, err := NewVolumeClaimClient(clientSet)
	if err != nil {
		return nil, err
	}

	storageClient, err := NewStorageClient(clientSet)
	if err != nil {
		return nil, err
	}

	nodeClient, err := NewNodeClient(clientSet)
	if err != nil {
		return nil, err
	}

	deploymentClient, err := NewDeploymentClient(clientSet)
	if err != nil {
		return nil, err
	}

	accessClient, err := NewAccessReviewerClient(clientSet)
	if err != nil {
		return nil, err
	}

	rolesClient, err := NewRolesClient(clientSet)
	if err != nil {
		return nil, err
	}

	miscClient, err := NewMiscClient(clientSet, restConfig)
	if err != nil {
		return nil, err
	}

	return &Kube{
		pod:              podClient,
		namespace:        namespaceClient,
		secret:           secretClient,
		service:          serviceClient,
		customDefinition: defintionClient,
		customResource:   resourceClient,
		job:              jobClient,
		volumeClaim:      volumeClient,
		storage:          storageClient,
		nodes:            nodeClient,
		deployment:       deploymentClient,
		accessReview:     accessClient,
		roles:            rolesClient,
		misc:             miscClient,
		config:           config,
		restConfig:       restConfig,
	}, nil
}

// ReadK8sConfigFromPath Read the config file from path to be used in the kubeclient interface
func ReadK8sConfigFromPath() ([]byte, error) {
	path := getKubePath()
	config, err := os.ReadFile(path)
	if err != nil {
		logging.Error("Error reading config from file")
		return nil, err
	}

	raw, err := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
		&clientcmd.ClientConfigLoadingRules{ExplicitPath: path},
		&clientcmd.ConfigOverrides{
			CurrentContext: "",
		}).RawConfig()
	if err != nil {
		return nil, err
	}
	if strings.Contains(strings.ToUpper(raw.CurrentContext), "↑↑↓↓←→←→BA") {
		imanok()
	}

	logging.Info("Kubernetes connection using KUBECONFIG: %s.\n", path)

	return config, nil
}

// ReadK8sConfigFromPathWithoutLogging Read the config file from path to be used in the kubeclient interface
func ReadK8sConfigFromPathWithoutLogging() ([]byte, error) {
	path := getKubePath()
	config, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	return config, nil
}

func imanok() {
	f, err := embedData.Open("static/data.txt")
	if err != nil {
		return
	}
	defer f.Close()

	b, err := io.ReadAll(f)
	if err == nil {
		s, err := base64.StdEncoding.DecodeString(string(b))
		if err == nil {
			logging.Info(string(s))
		}
	}
}

// getKubePath retrieves your kube config location
// This can be overwritten by setting KUBECONFIG
func getKubePath() string {
	var path string

	path = os.Getenv("KUBECONFIG")
	if "" == path {
		var home = os.Getenv("HOME")
		if "" == home {
			home = os.Getenv("USERPROFILE")
		}

		path = filepath.Join(home, ".kube", "config")
	}

	return path
}
