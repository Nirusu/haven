package kubernetes

import (
	"context"
	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	v1 "k8s.io/client-go/kubernetes/typed/apps/v1"
	"time"
)

type DeploymentImpl struct {
	client v1.AppsV1Interface
}

// NewDeploymentClient to interact with Deployment interface
func NewDeploymentClient(kube kubernetes.Interface) (*DeploymentImpl, error) {
	coreClient := kube.AppsV1()

	return &DeploymentImpl{client: coreClient}, nil
}

// List gets a list of pods from a ns - empty string will be treated as all
func (d *DeploymentImpl) List(namespace string) (*appsv1.DeploymentList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	deployments, err := d.client.Deployments(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return deployments, nil
}

// Delete will delete a deployment from a ns
func (d *DeploymentImpl) Delete(namespace, name string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	err := d.client.Deployments(namespace).Delete(ctx, name, metav1.DeleteOptions{})
	if err != nil {
		return err
	}

	return nil
}

// Create will create a deployment for a ns
func (d *DeploymentImpl) Create(namespace string, deployment *appsv1.Deployment) (*appsv1.Deployment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	deployment, err := d.client.Deployments(namespace).Create(ctx, deployment, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	return deployment, nil
}
