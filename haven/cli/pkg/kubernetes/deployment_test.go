package kubernetes

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDeploymentClient(t *testing.T) {
	t.Run("Create", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expectedName := "testdeployment"
		ns := "test"

		deployObject := createDeploymentObject(expectedName, ns)

		deploy, err := testClient.Deployments().Create(ns, deployObject)
		assert.Nil(t, err)
		assert.Equal(t, expectedName, deploy.Name)
		assert.Equal(t, ns, deploy.Namespace)
	})

	t.Run("CreatingTwiceReturnsError", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expectedName := "testdeployment"
		ns := "test"

		deployObject := createDeploymentObject(expectedName, ns)

		_, err = testClient.Deployments().Create(ns, deployObject)
		assert.Nil(t, err)
		_, err = testClient.Deployments().Create(ns, deployObject)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), expectedName)
	})

	t.Run("List", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expected := "testdeployment"
		expectedNameDeployOne := fmt.Sprintf("%sone", expected)
		expectedNameDeployTwo := fmt.Sprintf("%stwo", expected)
		ns := "test"

		// we need to create the deployments first
		err = CreateDeploymentForTest(expectedNameDeployOne, ns, testClient)
		assert.Nil(t, err)
		err = CreateDeploymentForTest(expectedNameDeployTwo, ns, testClient)
		assert.Nil(t, err)

		deploys, err := testClient.Deployments().List(ns)
		assert.Nil(t, err)
		assert.Equal(t, 2, len(deploys.Items))
		for _, deploy := range deploys.Items {
			assert.Equal(t, deploy.Namespace, ns)
			assert.Contains(t, deploy.Name, expected)

		}
	})
}
