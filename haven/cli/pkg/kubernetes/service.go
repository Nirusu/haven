package kubernetes

import (
	"context"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	v1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"time"
)

type ServiceImpl struct {
	client v1.CoreV1Interface
}

// NewServiceClient to interact with Service interface
func NewServiceClient(kube kubernetes.Interface) (*ServiceImpl, error) {
	coreClient := kube.CoreV1()

	return &ServiceImpl{client: coreClient}, nil
}

// List all services in your kube cluster
func (s *ServiceImpl) List(namespace string) (*corev1.ServiceList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	services, err := s.client.Services(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return services, nil
}

// Get a service in your kube cluster
func (s *ServiceImpl) Get(namespace, serviceName string) (*corev1.Service, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	service, err := s.client.Services(namespace).Get(ctx, serviceName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return service, nil
}

// Create a service in your kube cluster
func (s *ServiceImpl) Create(namespace string, svc *corev1.Service) (*corev1.Service, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	service, err := s.client.Services(namespace).Create(ctx, svc, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	return service, nil
}

// Delete removes a service from your kube cluster
func (s *ServiceImpl) Delete(namespace, serviceName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	return s.client.Services(namespace).Delete(ctx, serviceName, metav1.DeleteOptions{})
}
