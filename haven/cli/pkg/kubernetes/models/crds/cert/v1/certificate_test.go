package v1

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCertificateParsing(t *testing.T) {
	name := "testObject"
	namespace := "testNamespace"
	asStruct := CertificateResource{
		Object: Object{
			APIVersion: "",
			Kind:       "",
			Metadata: Metadata{
				CreationTimestamp: "",
				Generation:        0,
				Name:              name,
				Namespace:         namespace,
				ResourceVersion:   "",
				Uid:               "",
			},
			Spec: Spec{
				DNSNames: nil,
				IssuerRef: IssuerRef{
					Group: "",
					Kind:  "",
					Name:  "",
				},
				SecretName: "",
				Usages:     []string{"something", "somethingelse"},
			},
			Status: Status{
				Conditions: []Condition{{
					LastTransitionTime: "",
					Message:            "",
					ObservedGeneration: 0,
					Reason:             "",
					Status:             "",
					Type:               "",
				}},
				NotAfter:    "",
				NotBefore:   "",
				RenewalTime: "",
				Revision:    0,
			},
		},
	}

	t.Run("UnmarshallFromJson", func(t *testing.T) {
		asJson, err := asStruct.Marshal()
		assert.Nil(t, err)

		sut, err := UnmarshalCertificateResource(asJson)
		assert.Nil(t, err)
		assert.Equal(t, name, sut.Object.Metadata.Name)
		assert.Equal(t, namespace, sut.Object.Metadata.Namespace)
	})

	t.Run("MarshallToJson", func(t *testing.T) {
		sut, err := asStruct.Marshal()
		assert.Nil(t, err)
		assert.Contains(t, string(sut), name)
		assert.Contains(t, string(sut), namespace)
	})
}
