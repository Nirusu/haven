package v1alpha

import (
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
)

func TestGenerator(t *testing.T) {
	t.Run("StringsFromStruct", func(t *testing.T) {
		expected := `"testInput": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"commandline": {Type: "string"},
		"kubeHost": {Type: "string"},
		"platform": {Type: "string"},
	},
	Required: []string{
		"created",
		"version",
		"compliant",
	},
},`
		type TestInput struct {
			metav1.TypeMeta   `json:",inline"`
			metav1.ObjectMeta `json:"metadata,omitempty"`
			Commandline       string `json:"commandline"`
			KubeHost          string `json:"kubeHost"`
			Platform          string `json:"platform"`
		}

		testObject := TestInput{}
		sut, err := generateFromStruct(testObject, false, false)

		assert.Nil(t, err)
		assert.NotNil(t, sut)
		assert.Equal(t, expected, sut)
	})

	t.Run("StringsAndBoolsFromStruct", func(t *testing.T) {
		expected := `"testInput": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"Commandline": {Type: "string"},
		"KubeHost": {Type: "string"},
		"Platform": {Type: "boolean"},
	},
	Required: []string{
		"created",
		"version",
		"compliant",
	},
},`
		type TestInput struct {
			metav1.TypeMeta   `json:",inline"`
			metav1.ObjectMeta `json:"metadata,omitempty"`
			Commandline       string `json:"Commandline"`
			KubeHost          string `json:"KubeHost"`
			Platform          bool   `json:"Platform"`
		}

		testObject := TestInput{}
		sut, err := generateFromStruct(testObject, false, false)

		assert.Nil(t, err)
		assert.NotNil(t, sut)
		assert.Equal(t, expected, sut)
	})

	t.Run("StringsAndBoolsandIntsFromStruct", func(t *testing.T) {
		expected := `"testInput": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"Commandline": {Type: "string"},
		"KubeHost": {Type: "string"},
		"Platform": {Type: "boolean"},
		"Total": {Type: "number"},
	},
	Required: []string{
		"created",
		"version",
		"compliant",
	},
},`
		type TestInput struct {
			metav1.TypeMeta   `json:",inline"`
			metav1.ObjectMeta `json:"metadata,omitempty"`
			Commandline       string `json:"Commandline"`
			KubeHost          string `json:"KubeHost"`
			Platform          bool   `json:"Platform"`
			Total             int64  `json:"Total"`
		}

		testObject := TestInput{}
		sut, err := generateFromStruct(testObject, false, false)

		assert.Nil(t, err)
		assert.NotNil(t, sut)
		assert.Equal(t, expected, sut)
	})

	t.Run("NestedStructs", func(t *testing.T) {
		expected := `"testInput": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
"testPlatform": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"Proxy": {Type: "boolean"},
		"DeterminingLabel": {Type: "string"},
		"DeterminedPlatform": {Type: "string"},
	},
},
		"Commandline": {Type: "string"},
		"KubeHost": {Type: "string"},
	},
	Required: []string{
		"created",
		"version",
		"compliant",
	},
},`

		type TestPlatform struct {
			metav1.TypeMeta    `json:",inline"`
			metav1.ObjectMeta  `json:"metadata,omitempty"`
			Proxy              bool   `json:"Proxy"`
			DeterminingLabel   string `json:"DeterminingLabel"`
			DeterminedPlatform string `json:"DeterminedPlatform"`
		}

		type TestInput struct {
			metav1.TypeMeta   `json:",inline"`
			metav1.ObjectMeta `json:"metadata,omitempty"`
			Commandline       string       `json:"Commandline"`
			KubeHost          string       `json:"KubeHost"`
			Platform          TestPlatform `json:"Platform"`
		}

		testObject := TestInput{}
		sut, err := generateFromStruct(testObject, false, false)

		assert.Nil(t, err)
		assert.NotNil(t, sut)
		assert.Equal(t, expected, sut)
	})

	t.Run("NestedTripleStructs", func(t *testing.T) {
		expected := `"testInput": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
"testPlatform": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
"testConfig": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"CNCF": {Type: "boolean"},
		"CIS": {Type: "boolean"},
	},
},
		"Proxy": {Type: "boolean"},
		"DeterminingLabel": {Type: "string"},
		"DeterminedPlatform": {Type: "string"},
	},
},
		"Commandline": {Type: "string"},
		"KubeHost": {Type: "string"},
	},
	Required: []string{
		"created",
		"version",
		"compliant",
	},
},`

		type TestConfig struct {
			Cncf bool `json:"CNCF"`
			Cis  bool `json:"CIS"`
		}

		type TestPlatform struct {
			metav1.TypeMeta    `json:",inline"`
			metav1.ObjectMeta  `json:"metadata,omitempty"`
			Proxy              bool       `json:"Proxy"`
			DeterminingLabel   string     `json:"DeterminingLabel"`
			DeterminedPlatform string     `json:"DeterminedPlatform"`
			Config             TestConfig `json:"Config"`
		}

		type TestInput struct {
			metav1.TypeMeta   `json:",inline"`
			metav1.ObjectMeta `json:"metadata,omitempty"`
			Commandline       string       `json:"Commandline"`
			KubeHost          string       `json:"KubeHost"`
			Platform          TestPlatform `json:"Platform"`
		}

		testObject := TestInput{}
		sut, err := generateFromStruct(testObject, false, false)

		assert.Nil(t, err)
		assert.NotNil(t, sut)
		assert.Equal(t, expected, sut)
	})

	t.Run("SliceInObject", func(t *testing.T) {
		expected := `"testInput": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"Results": { 
	Type: "array", 															
	Items: &apiextensionv1.JSONSchemaPropsOrArray{
    Schema: &apiextensionv1.JSONSchemaProps{
			Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"Name": {Type: "string"},
		"Config": { 
	Type: "array", 															
	Items: &apiextensionv1.JSONSchemaPropsOrArray{
    Schema: &apiextensionv1.JSONSchemaProps{
			Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"CNCF": {Type: "boolean"},
		"CIS": {Type: "boolean"},
	},
},
		},
	},
	},
},
		},
	},
		"kubeHost": {Type: "string"},
	},
	Required: []string{
		"created",
		"version",
		"compliant",
	},
},`

		type TestConfig struct {
			Cncf bool `json:"CNCF"`
			Cis  bool `json:"CIS"`
		}

		type TestResultElement struct {
			Name   string       `json:"Name"`
			Config []TestConfig `json:"Config"`
		}

		type TestInput struct {
			metav1.TypeMeta   `json:",inline"`
			metav1.ObjectMeta `json:"metadata,omitempty"`
			Results           []TestResultElement `json:"Results"`
			KubeHost          string              `json:"kubeHost"`
		}

		testObject := TestInput{}
		sut, err := generateFromStruct(testObject, false, false)

		assert.Nil(t, err)
		assert.NotNil(t, sut)
		assert.Equal(t, expected, sut)
	})

	t.Run("ObjectInSlice", func(t *testing.T) {
		expected := `"testInput": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"Results": { 
	Type: "array", 															
	Items: &apiextensionv1.JSONSchemaPropsOrArray{
    Schema: &apiextensionv1.JSONSchemaProps{
			Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"Name": {Type: "string"},
		"Label": {Type: "string"},
		"Result": {Type: "string"},
	},
},
		},
	},
		"kubeHost": {Type: "string"},
	},
	Required: []string{
		"created",
		"version",
		"compliant",
	},
},`

		type TestResultElement struct {
			Name   string `json:"Name"`
			Label  string `json:"Label"`
			Result string `json:"Result"`
		}

		type TestInput struct {
			metav1.TypeMeta   `json:",inline"`
			metav1.ObjectMeta `json:"metadata,omitempty"`
			Results           []TestResultElement `json:"Results"`
			KubeHost          string              `json:"kubeHost"`
		}

		testObject := TestInput{}
		sut, err := generateFromStruct(testObject, false, false)

		assert.Nil(t, err)
		assert.NotNil(t, sut)
		assert.Equal(t, expected, sut)
	})

	t.Run("CreateBody", func(t *testing.T) {
		expected := `// CreateDefinition creates a new CRD specific to haven
func CreateDefinition() *apiextensionv1.CustomResourceDefinition {
	crd := &apiextensionv1.CustomResourceDefinition{
		ObjectMeta: metav1.ObjectMeta{Name: Name},
		Spec: apiextensionv1.CustomResourceDefinitionSpec{
			Group: GroupName,
			Scope: apiextensionv1.ClusterScoped,
			Names: apiextensionv1.CustomResourceDefinitionNames{
				Plural:     Plural,
				Singular:   Singular,
				ShortNames: []string{ShortName},
				Kind:       Kind,
			},
			Versions: []apiextensionv1.CustomResourceDefinitionVersion{
				{
					Name:    Version,
					Served:  true,
					Storage: true,
					Schema: &apiextensionv1.CustomResourceValidation{
						OpenAPIV3Schema: &apiextensionv1.JSONSchemaProps{
	Type: "object", 
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"spec": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
"input": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
"platform": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"proxy": {Type: "boolean"},
		"determiningLabel": {Type: "string"},
		"determinedPlatform": {Type: "string"},
	},
},
		"commandline": {Type: "string"},
		"kubeHost": {Type: "string"},
	},
},
"output": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
"config": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"CNCF": {Type: "boolean"},
		"CIS": {Type: "boolean"},
	},
},
"compliancyChecks": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
"summary": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"total": {Type: "number"},
		"unknown": {Type: "number"},
		"skipped": {Type: "number"},
		"failed": {Type: "number"},
		"passed": {Type: "number"},
	},
},
		"results": { 
	Type: "array", 															
	Items: &apiextensionv1.JSONSchemaPropsOrArray{
    Schema: &apiextensionv1.JSONSchemaProps{
			Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"name": {Type: "string"},
		"label": {Type: "string"},
		"category": {Type: "string"},
		"rationale": {Type: "string"},
		"result": {Type: "string"},
	},
},
		},
	},
	},
},
"suggestedChecks": {
	Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"results": { 
	Type: "array", 															
	Items: &apiextensionv1.JSONSchemaPropsOrArray{
    Schema: &apiextensionv1.JSONSchemaProps{
			Type: "object",
	Properties: map[string]apiextensionv1.JSONSchemaProps{
		"name": {Type: "string"},
		"label": {Type: "string"},
		"category": {Type: "string"},
		"rationale": {Type: "string"},
		"result": {Type: "string"},
	},
},
		},
	},
	},
},
		"version": {Type: "string"},
		"havenCompliant": {Type: "boolean"},
		"startTS": {Type: "string"},
		"stopTS": {Type: "string"},
	},
},
		"compliant": {Type: "boolean"},
		"created": {Type: "string"},
		"version": {Type: "string"},
	},
	Required: []string{
		"created",
		"version",
		"compliant",
	},
},
		},
	},
},
		AdditionalPrinterColumns: []apiextensionv1.CustomResourceColumnDefinition{
	{
		Name:     "version",
		Type:     "string",
		JSONPath: ".spec.version",
	},
	{
		Name:     "compliant",
		Type:     "boolean",
		JSONPath: ".spec.compliant",
	},
},
},
		},
		},
	}

	return crd
}`
		sut, err := createBody()

		assert.Nil(t, err)
		assert.NotNil(t, sut)
		assert.Equal(t, expected, sut)
	})

	t.Run("CreateFormattedFileContent", func(t *testing.T) {
		expected := `//go:build !ignore_autogenerated
// +build !ignore_autogenerated

// Code generated by Haven tooling. DO NOT EDIT.

package v1alpha

import (
	apiextensionv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// CreateDefinition creates a new CRD specific to haven
func CreateDefinition() *apiextensionv1.CustomResourceDefinition {
	crd := &apiextensionv1.CustomResourceDefinition{
		ObjectMeta: metav1.ObjectMeta{Name: Name},
		Spec: apiextensionv1.CustomResourceDefinitionSpec{
			Group: GroupName,
			Scope: apiextensionv1.ClusterScoped,
			Names: apiextensionv1.CustomResourceDefinitionNames{
				Plural:     Plural,
				Singular:   Singular,
				ShortNames: []string{ShortName},
				Kind:       Kind,
			},
			Versions: []apiextensionv1.CustomResourceDefinitionVersion{
				{
					Name:    Version,
					Served:  true,
					Storage: true,
					Schema: &apiextensionv1.CustomResourceValidation{
						OpenAPIV3Schema: &apiextensionv1.JSONSchemaProps{
							Type: "object",
							Properties: map[string]apiextensionv1.JSONSchemaProps{
								"spec": {
									Type: "object",
									Properties: map[string]apiextensionv1.JSONSchemaProps{
										"input": {
											Type: "object",
											Properties: map[string]apiextensionv1.JSONSchemaProps{
												"platform": {
													Type: "object",
													Properties: map[string]apiextensionv1.JSONSchemaProps{
														"proxy":              {Type: "boolean"},
														"determiningLabel":   {Type: "string"},
														"determinedPlatform": {Type: "string"},
													},
												},
												"commandline": {Type: "string"},
												"kubeHost":    {Type: "string"},
											},
										},
										"output": {
											Type: "object",
											Properties: map[string]apiextensionv1.JSONSchemaProps{
												"config": {
													Type: "object",
													Properties: map[string]apiextensionv1.JSONSchemaProps{
														"CNCF": {Type: "boolean"},
														"CIS":  {Type: "boolean"},
													},
												},
												"compliancyChecks": {
													Type: "object",
													Properties: map[string]apiextensionv1.JSONSchemaProps{
														"summary": {
															Type: "object",
															Properties: map[string]apiextensionv1.JSONSchemaProps{
																"total":   {Type: "number"},
																"unknown": {Type: "number"},
																"skipped": {Type: "number"},
																"failed":  {Type: "number"},
																"passed":  {Type: "number"},
															},
														},
														"results": {
															Type: "array",
															Items: &apiextensionv1.JSONSchemaPropsOrArray{
																Schema: &apiextensionv1.JSONSchemaProps{
																	Type: "object",
																	Properties: map[string]apiextensionv1.JSONSchemaProps{
																		"name":      {Type: "string"},
																		"label":     {Type: "string"},
																		"category":  {Type: "string"},
																		"rationale": {Type: "string"},
																		"result":    {Type: "string"},
																	},
																},
															},
														},
													},
												},
												"suggestedChecks": {
													Type: "object",
													Properties: map[string]apiextensionv1.JSONSchemaProps{
														"results": {
															Type: "array",
															Items: &apiextensionv1.JSONSchemaPropsOrArray{
																Schema: &apiextensionv1.JSONSchemaProps{
																	Type: "object",
																	Properties: map[string]apiextensionv1.JSONSchemaProps{
																		"name":      {Type: "string"},
																		"label":     {Type: "string"},
																		"category":  {Type: "string"},
																		"rationale": {Type: "string"},
																		"result":    {Type: "string"},
																	},
																},
															},
														},
													},
												},
												"version":        {Type: "string"},
												"havenCompliant": {Type: "boolean"},
												"startTS":        {Type: "string"},
												"stopTS":         {Type: "string"},
											},
										},
										"compliant": {Type: "boolean"},
										"created":   {Type: "string"},
										"version":   {Type: "string"},
									},
									Required: []string{
										"created",
										"version",
										"compliant",
									},
								},
							},
						},
					},
					AdditionalPrinterColumns: []apiextensionv1.CustomResourceColumnDefinition{
						{
							Name:     "version",
							Type:     "string",
							JSONPath: ".spec.version",
						},
						{
							Name:     "compliant",
							Type:     "boolean",
							JSONPath: ".spec.compliant",
						},
					},
				},
			},
		},
	}

	return crd
}
`
		sut := createFormattedFileContent()

		assert.NotNil(t, sut)

		stringifiedSut := string(sut)
		assert.Equal(t, expected, stringifiedSut)
	})

	t.Run("FindPath", func(t *testing.T) {
		expected := "haven/cli/pkg/kubernetes/models/crds/haven/v1alpha/zz_generated_compliancy_definition.go"

		sut := findPath()

		assert.NotNil(t, sut)
		assert.Contains(t, sut, expected)
	})

}
