package v1alpha

/*
10-12-2021
This package contains haven's own CustomResources

In order to install these and use them directly you need to generate DeepCopy() functions.
To make this generation a lot easier and less error-prone a generate method has been added here.
It uses the controller-gen tool that has been created for this very purpose. This needs to be available locally and can be installed by using:

go get sigs.k8s.io/controller-tools/cmd/controller-gen

You can now generate using

$ go generate ...

Each struct within the CR needs to be annotated and generated, they also need a metav1.TypeMeta and metav1.ListMeta as they become K8s objects.

For convenience the zz_generated_deepcopy.go has been added to the repo.
*/

import (
	"encoding/json"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

//go:generate controller-gen object paths=$GOFILE

func UnmarshalCompliancy(data []byte) (Compliancy, error) {
	var r Compliancy
	err := json.Unmarshal(data, &r)
	return r, err
}

func (c *Compliancy) Marshal() ([]byte, error) {
	return json.Marshal(c)
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type CompliancyList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Compliancy `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type Compliancy struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	APIVersion        string `json:"apiVersion"`
	Kind              string `json:"kind"`
	Spec              Spec   `json:"spec"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type Spec struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Compliant         bool   `json:"compliant"`
	Created           string `json:"created"`
	Version           string `json:"version"`
	Input             Input  `json:"input"`
	Output            Output `json:"output"`
}

func UnmarshalOutputResource(data []byte) (Output, error) {
	var r Output
	err := json.Unmarshal(data, &r)
	return r, err
}

func UnmarshalInputResource(data []byte) (Input, error) {
	var r Input
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Output) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type Output struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Version           string           `json:"version"`
	HavenCompliant    bool             `json:"havenCompliant"`
	StartTS           string           `json:"startTS"`
	StopTS            string           `json:"stopTS"`
	Config            Config           `json:"config"`
	CompliancyChecks  CompliancyChecks `json:"compliancyChecks"`
	SuggestedChecks   SuggestedChecks  `json:"suggestedChecks"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type Input struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Commandline       string   `json:"commandline"`
	KubeHost          string   `json:"kubeHost"`
	Platform          Platform `json:"platform"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type Platform struct {
	metav1.TypeMeta    `json:",inline"`
	metav1.ObjectMeta  `json:"metadata,omitempty"`
	Proxy              bool   `json:"proxy"`
	DeterminingLabel   string `json:"determiningLabel"`
	DeterminedPlatform string `json:"determinedPlatform"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type CompliancyChecks struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Results           []ResultElement `json:"results"`
	Summary           Summary         `json:"summary"`
}

type ResultElement struct {
	Name      string     `json:"name"`
	Label     string     `json:"label"`
	Category  string     `json:"category"`
	Rationale string     `json:"rationale"`
	Result    ResultEnum `json:"result"`
}

type Summary struct {
	Total   int64 `json:"total"`
	Unknown int64 `json:"unknown"`
	Skipped int64 `json:"skipped"`
	Failed  int64 `json:"failed"`
	Passed  int64 `json:"passed"`
}

type Config struct {
	Cncf bool `json:"CNCF"`
	Cis  bool `json:"CIS"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type SuggestedChecks struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Results           []ResultElement `json:"results"`
}

type ResultEnum string

const (
	No      ResultEnum = "NO"
	Skipped ResultEnum = "SKIPPED"
	Yes     ResultEnum = "YES"
)
