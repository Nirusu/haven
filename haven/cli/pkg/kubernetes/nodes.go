package kubernetes

import (
	"context"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	v1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"time"
)

type NodeImpl struct {
	client v1.CoreV1Interface
}

// NewNodeClient to interact with Nodes interface
func NewNodeClient(kube kubernetes.Interface) (*NodeImpl, error) {
	coreClient := kube.CoreV1()

	return &NodeImpl{client: coreClient}, nil
}

// List will give an overview of nodes within your kubecluster
// master nodes in managed kubernetes cannot be listed
func (n *NodeImpl) List() (*corev1.NodeList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	nodes, err := n.client.Nodes().List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return nodes, err
}

// Create will create a node in your kubecluster
// master nodes in managed kubernetes cannot be created
func (n *NodeImpl) Create(node *corev1.Node) (*corev1.Node, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	created, err := n.client.Nodes().Create(ctx, node, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	return created, err
}
