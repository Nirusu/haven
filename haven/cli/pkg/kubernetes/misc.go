package kubernetes

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/version"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

type MiscImpl struct {
	client kubernetes.Interface
	rest   *rest.Config
}

// NewMiscClient to interact with Misc interface
func NewMiscClient(kube kubernetes.Interface, rest *rest.Config) (*MiscImpl, error) {
	return &MiscImpl{client: kube, rest: rest}, nil
}

// GetServerVersion retrieves the version info for your client
func (m *MiscImpl) GetServerVersion() (*version.Info, error) {
	kubeServerSrc, err := m.client.Discovery().ServerVersion()
	if err != nil {
		return nil, err
	}

	return kubeServerSrc, nil
}

// GetHost retrieves the api host in your kube cli
func (m *MiscImpl) GetHost() string {
	return m.rest.Host
}

// GetServerGroups uses the discovery client to return server groups
func (m *MiscImpl) GetServerGroups() (*metav1.APIGroupList, error) {
	groupList, err := m.client.Discovery().ServerGroups()
	if err != nil {
		return nil, err
	}

	return groupList, nil
}
