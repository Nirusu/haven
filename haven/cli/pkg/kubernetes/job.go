package kubernetes

import (
	"context"
	batchv1 "k8s.io/api/batch/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	v1 "k8s.io/client-go/kubernetes/typed/batch/v1"
	"time"
)

type JobImpl struct {
	client v1.BatchV1Interface
}

// NewJobClient to interact with Job interface
func NewJobClient(kube kubernetes.Interface) (*JobImpl, error) {
	batchClient := kube.BatchV1()

	return &JobImpl{client: batchClient}, nil
}

// List gets a list of jobs from a ns - empty string will be treated as all
func (j *JobImpl) List(namespace string) (*batchv1.JobList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	jobs, err := j.client.Jobs(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return jobs, nil
}

// Delete will delete a named job from a namespace
func (j *JobImpl) Delete(namespace, jobName string, pp *metav1.DeletionPropagation) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	policy := pp
	if pp == nil {
		bg := metav1.DeletePropagationBackground
		policy = &bg
	}

	err := j.client.Jobs(namespace).Delete(ctx, jobName, metav1.DeleteOptions{PropagationPolicy: policy})
	return err
}

// Create will create a named job from a namespace
func (j *JobImpl) Create(namespace string, job *batchv1.Job) (*batchv1.Job, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	job, err := j.client.Jobs(namespace).Create(ctx, job, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}
	return job, err
}
