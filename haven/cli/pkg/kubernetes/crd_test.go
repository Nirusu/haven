package kubernetes

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCrdClient(t *testing.T) {
	t.Run("Create", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expectedName := "testcrd"
		crdObject := createCrdObject(expectedName)
		assert.Nil(t, err)

		crd, err := testClient.CustomResourceDefinitions().Create(crdObject)
		assert.Nil(t, err)
		assert.Equal(t, expectedName, crd.Name)
		assert.Equal(t, expectedName, crd.Spec.Names.Singular)
	})

	t.Run("CreatingTwiceReturnsError", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expectedName := "testcrd"
		crdObject := createCrdObject(expectedName)
		assert.Nil(t, err)

		_, err = testClient.CustomResourceDefinitions().Create(crdObject)
		assert.Nil(t, err)
		_, err = testClient.CustomResourceDefinitions().Create(crdObject)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), expectedName)
	})

	t.Run("Get", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expected := "testcrd"
		expectedNameCrdOne := fmt.Sprintf("%sone", expected)
		expectedNameCrdTwo := fmt.Sprintf("%stwo", expected)

		err = CreateCrdForTest(expectedNameCrdOne, testClient)
		assert.Nil(t, err)
		err = CreateCrdForTest(expectedNameCrdTwo, testClient)
		assert.Nil(t, err)

		crds, err := testClient.CustomResourceDefinitions().List()
		assert.Nil(t, err)
		assert.True(t, len(crds.Items) == 2)

		for _, crd := range crds.Items {
			assert.Contains(t, crd.Name, expected)
			assert.Contains(t, crd.Spec.Names.Singular, expected)
		}
	})

}
