package kubernetes

import (
	"context"
	authorizationv1 "k8s.io/api/authorization/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	v1 "k8s.io/client-go/kubernetes/typed/authorization/v1"
	"time"
)

type AccessReviewImpl struct {
	client v1.AuthorizationV1Interface
}

// NewAccessReviewerClient to interact with AccessReview interface
func NewAccessReviewerClient(kube kubernetes.Interface) (*AccessReviewImpl, error) {
	coreClient := kube.AuthorizationV1()

	return &AccessReviewImpl{client: coreClient}, nil
}

//Create creates a new AccessReview
func (a *AccessReviewImpl) Create(sar *authorizationv1.SelfSubjectAccessReview) (*authorizationv1.SelfSubjectAccessReview, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	ssar, err := a.client.SelfSubjectAccessReviews().Create(ctx, sar, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	return ssar, nil
}
