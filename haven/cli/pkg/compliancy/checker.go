// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package compliancy

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/Masterminds/semver/v3"
	"github.com/gookit/color"
	"github.com/olekukonko/tablewriter"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/haven"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/models/crds/haven/v1alpha"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/sonobuoy"
)

// Checker runs the compliancy checks and returns a list of results
type Checker struct {
	config   *Config
	platform v1alpha.Platform
}

type Platform string

const (
	PlatformUnknown   Platform = "Unknown"
	PlatformEKS       Platform = "Amazon AWS EKS"
	PlatformAKS       Platform = "Microsoft Azure AKS"
	PlatformGKE       Platform = "Google Cloud Platform GKE"
	PlatformOpenShift Platform = "Red Hat OpenShift"
	PlatformARO       Platform = "Azure Red Hat OpenShift (managed service)"
	AksImageLabel     string   = "kubernetes.azure.com/node-image-version"
	AksRole           string   = "kubernetes.azure.com/role"
	AksAgent          string   = "agent"
	EksKey            string   = "eks"
	GkeKey            string   = "gke"
	AksKey            string   = "aks"
	NamespacePrefix   string   = "haven-check"
)

type HavenRelease struct {
	Date    time.Time `json:"date"`
	Version string    `json:"version"`
}

type HavenReleases struct {
	Releases []HavenRelease `json:"releases"`
}

func (h HavenReleases) latest() HavenRelease {
	var hr HavenRelease

	lv, _ := semver.NewVersion("v0.0.0")

	for _, item := range h.Releases {
		cv, err := semver.NewVersion(item.Version)
		if err == nil {
			if cv.GreaterThan(lv) {
				lv = cv
				hr = item
			}
		}
	}

	return hr
}

func (h HavenReleases) findByVersion(version string) HavenRelease {
	var hr HavenRelease

	for _, item := range h.Releases {
		if item.Version == version {
			hr = item
		}
	}

	return hr
}

// NewChecker returns a new Checker
func NewChecker(cncfConfigured bool, runCISChecks bool) (*Checker, error) {
	platform := v1alpha.Platform{}

	kubePath, _ := kubernetes.ReadK8sConfigFromPath()
	kube, err := kubernetes.NewClientFromFile(kubePath)
	if err != nil {
		return nil, err
	}

	crdClient, err := haven.NewCrdConfig(kubePath)
	if err != nil {
		return nil, err
	}

	kubeServerSrc, err := kube.Misc().GetServerVersion()
	if err != nil {
		return nil, err
	}

	kubeServer, err := semver.NewVersion(kubeServerSrc.String())
	if err != nil {
		return nil, fmt.Errorf("Kubernetes server version: %s, Error: %s", kubeServerSrc.String(), err)
	}

	determindedPlatform, err := BestEffortPlatformCheck(kube, &platform)
	if err != nil {
		return nil, err
	}

	havenReleases, err := FetchHavenReleases()
	if err != nil {
		return nil, err
	}

	kubeLatest, err := LatestKubernetesVersion()
	if err != nil {
		return nil, err
	}

	platform.DeterminedPlatform = string(determindedPlatform)

	ns := fmt.Sprintf("%s-%d", NamespacePrefix, time.Now().Unix())

	appName := "kube-bench-" + strings.ToLower(base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(time.Now().Nanosecond())))[:5])

	sono := sonobuoy.NewSonobuoyClient(sonobuoy.DefaultMode)

	globalInterval := 2500 * time.Millisecond

	return &Checker{
		config: &Config{
			HavenReleases:   havenReleases,
			KubeServer:      kubeServer,
			KubeLatest:      kubeLatest,
			CNCFConfigured:  cncfConfigured,
			RunCISChecks:    runCISChecks,
			HostPlatform:    determindedPlatform,
			Namespace:       ns,
			Kube:            kube,
			CrdClient:       crdClient,
			CisCheckAppName: appName,
			GlobalInterval:  globalInterval,
			Sono:            sono,
		},
		platform: platform,
	}, nil
}

func FetchHavenReleases() (HavenReleases, error) {
	var havenReleases HavenReleases

	res, err := http.Get("https://haven.commonground.nl/releases.json")
	if err != nil {
		return havenReleases, fmt.Errorf("Could not fetch latest Haven releases: '%s'", err.Error())
	}
	defer res.Body.Close()

	content, err := io.ReadAll(res.Body)
	if err != nil {
		return havenReleases, fmt.Errorf("Could not read latest Haven releases: '%s'", err.Error())
	}

	err = json.Unmarshal([]byte(content), &havenReleases)
	if err != nil {
		return havenReleases, fmt.Errorf("Could not parse latest Haven releases: '%s'", err.Error())
	}

	return havenReleases, nil
}

func LatestKubernetesVersion() (string, error) {
	res, err := http.Get("https://storage.googleapis.com/kubernetes-release/release/stable.txt")
	if err != nil {
		return "", fmt.Errorf("Could not fetch latest Kubernetes release: '%s'", err.Error())
	}
	defer res.Body.Close()

	content, err := io.ReadAll(res.Body)
	if err != nil {
		return "", fmt.Errorf("Could not read latest Kubernetes release: '%s'", err.Error())
	}

	return string(content), nil
}

func BestEffortPlatformCheck(kube kubernetes.KubeClient, platform *v1alpha.Platform) (Platform, error) {
	determinedPlatform := PlatformUnknown

	awsMasterPattern := regexp.MustCompile(`https:\/\/.*.eks.amazonaws.com`)
	if awsMasterPattern.MatchString(kube.Misc().GetHost()) {
		determinedPlatform = PlatformEKS
	}

	azureMasterPattern := regexp.MustCompile(`https:\/\/.*.azmk8s.io`)
	if azureMasterPattern.MatchString(kube.Misc().GetHost()) {
		determinedPlatform = PlatformAKS
	}

	serverVersion, err := kube.Misc().GetServerVersion()
	if err != nil {
		return determinedPlatform, fmt.Errorf("Error determining server version: %s", err)
	}

	googleMasterVersionPattern := regexp.MustCompile(`gke`)
	if googleMasterVersionPattern.MatchString(serverVersion.String()) {
		determinedPlatform = PlatformGKE
	}

	namespaces, err := kube.Namespaces().List()
	if err == nil {
		// Loops over all namespaces and sets plaform to PlatformOpenShift if
		// openshift-apiserver namespace is found.
		for _, ns := range namespaces.Items {
			if "openshift-apiserver" == ns.Name {
				determinedPlatform = PlatformOpenShift
			}
		}
		// Loops over all namespaces again and overrides platform to PlatformARO if
		// openshift-azure-logging namespace is found.
		for _, ns := range namespaces.Items {
			if "openshift-azure-logging" == ns.Name {
				determinedPlatform = PlatformARO
			}
		}
	}

	// if the platform is still not known we try to get it by viewing the labels attached to a node
	// this is used primarily when there is a platform behind a proxy such as rancher or pinniped
	if determinedPlatform == PlatformUnknown {
		nodes, err := kube.Nodes().List()
		if err != nil {
			return determinedPlatform, err
		}

		randomNodeNumber := rand.Intn(len(nodes.Items))

		node := nodes.Items[randomNodeNumber]

		p, err := BestEffortLabelCheck(node.Labels, platform)
		if err != nil {
			return determinedPlatform, err
		}

		if p != PlatformUnknown {
			determinedPlatform = p
			platform.Proxy = true
		}
	}

	return determinedPlatform, nil
}

func BestEffortLabelCheck(labels map[string]string, platform *v1alpha.Platform) (Platform, error) {
	determinedPlatform := PlatformUnknown

	for key, value := range labels {
		if key == AksRole {
			if value == AksAgent {
				determinedPlatform = PlatformAKS
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}
		if key == AksImageLabel {
			match := AksBasedImage(value)
			if match {
				determinedPlatform = PlatformAKS
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}

		if strings.Contains(key, EksKey) {
			match := EksBasedLabel(key)
			if match {
				determinedPlatform = PlatformEKS
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}

		if strings.Contains(key, GkeKey) {
			match := GkeBasedLabel(key)
			if match {
				determinedPlatform = PlatformGKE
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}
	}

	return determinedPlatform, nil
}

func AksBasedImage(value string) bool {
	regex := "^AKSUbuntu[a-zA-Z0-9\\-\\.]+$"
	re := regexp.MustCompile(regex)
	match := re.MatchString(value)

	return match
}

func EksBasedLabel(value string) bool {
	regex := "^eks\\.amazonaws\\.com+\\/[a-zA-Z0-9\\-]+$"
	re := regexp.MustCompile(regex)
	match := re.MatchString(value)

	return match
}

func GkeBasedLabel(value string) bool {
	regex := "^cloud\\.google\\.com\\/gke-[a-zA-Z0-9\\-]+$"
	re := regexp.MustCompile(regex)
	match := re.MatchString(value)

	return match
}

// Run the checker and return a slice of results
func (c *Checker) Run() error {
	for i := range checks.CompliancyChecks {
		if !c.config.CNCFConfigured && checks.CompliancyChecks[i].Name == "cncf" {
			checks.CompliancyChecks[i].Result = ResultSkipped
			continue
		}

		compliancyResult, err := checks.CompliancyChecks[i].Exec(c.config)
		if err != nil {
			logging.Fatal("Compliancy Check '%s' fatal error: \n    - '%s'\n\n", checks.CompliancyChecks[i].Label, err.Error())
		}

		checks.CompliancyChecks[i].Result = compliancyResult
	}

	for i := range checks.SuggestedChecks {
		if !c.config.RunCISChecks && checks.SuggestedChecks[i].Name == "cis" {
			checks.SuggestedChecks[i].Result = ResultSkipped
			continue
		}

		suggestedResult, err := checks.SuggestedChecks[i].Exec(c.config)
		if err != nil {
			logging.Fatal("Suggested Check '%s' fatal error: \n    - '%s'\n\n", checks.SuggestedChecks[i].Label, err.Error())
		}

		checks.SuggestedChecks[i].Result = suggestedResult
	}

	return nil
}

// PrintResults prints the table of checks with the corresponding results
func (c *Checker) PrintResults(checks []Check, compliancyChecks bool) {
	tableOut := &strings.Builder{}
	table := tablewriter.NewWriter(tableOut)

	table.SetHeader([]string{"Category", "Name", "Passed"})
	table.SetAutoWrapText(false)

	totalChecks := 0
	unknownChecks := 0
	skippedChecks := 0
	failedChecks := 0
	passedChecks := 0

	for _, check := range checks {
		totalChecks++

		switch check.Result {
		case ResultUnknown:
			unknownChecks++
		case ResultSkipped:
			skippedChecks++
		case ResultNo:
			failedChecks++
		case ResultYes:
			passedChecks++
		}

		table.Append([]string{string(check.Category), check.Label, check.Result.String()})
	}

	table.Render()

	prefix := "Compliancy checks results:\n"
	if compliancyChecks {
		compliancy := fmt.Sprintf("Results: %d out of %d checks passed, %d checks skipped, %d checks unknown.", passedChecks, totalChecks, skippedChecks, unknownChecks)

		if passedChecks == totalChecks {
			logging.Info(color.Green.Sprintf("%s This is a Haven Compliant cluster.\n", compliancy))

			output.HavenCompliant = true
		} else if failedChecks > 0 {
			logging.Error(color.Red.Sprintf("%s This is NOT a Haven Compliant cluster.\n", compliancy))
		} else {
			logging.Warning(color.Yellow.Sprintf("%s This COULD be a Haven Compliant cluster.\n", compliancy))
		}

		output.CompliancyChecks.Summary.Total = totalChecks
		output.CompliancyChecks.Summary.Passed = passedChecks
		output.CompliancyChecks.Summary.Failed = failedChecks
		output.CompliancyChecks.Summary.Skipped = skippedChecks
		output.CompliancyChecks.Summary.Unknown = unknownChecks
	} else {
		prefix = "Suggested checks results:\n"
	}

	logging.Info(fmt.Sprintf("%s\n%s\n", prefix, tableOut.String()))
}
