// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package compliancy

import (
	"encoding/json"
	"fmt"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/haven"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/models/crds/haven/v1alpha"
	"time"
)

type CompliancyOutput struct {
	Version        string
	HavenCompliant bool
	StartTS        time.Time
	StopTS         time.Time
	Config         struct {
		CNCF bool
		CIS  bool
	}
	CompliancyChecks struct {
		Results []Check
		Summary struct {
			Total   int
			Unknown int
			Skipped int
			Failed  int
			Passed  int
		}
	}
	SuggestedChecks struct {
		Results []Check
	}
}

type RationaleOutput struct {
	Version          string
	CompliancyChecks []Check
	SuggestedChecks  []Check
}

// outputJson returns a JSON object as a string.
func outputJson(output interface{}) (string, error) {
	b, err := json.Marshal(output)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s\n", string(b)), nil
}

// persistInCluster writes the Compliancy output to the cluster using the Haven CRD.
func persistInCluster(crdClient haven.V1Alpha1, compliant bool, output string, input v1alpha.Input) error {
	compliancyResource, err := crdClient.CreateHavenResource(output, input, compliant)
	if err != nil {
		return err
	}

	_, err = crdClient.Create(compliancyResource)
	if err != nil {
		return err
	}

	return nil
}
