// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package compliancy

import (
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// RunCommandOnPod takes a string as command input and returns the output from
// running that command from a privileged pod, unless this results in an error.
// It's possible to force scheduling of the pod on a master node when needed.
func RunCommandOnPod(config *Config, useMaster bool, cmd string) (string, error) {
	// Find a master node to work with.
	nodes, err := config.Kube.Nodes().List()
	if err != nil {
		return "", err
	}

	var master apiv1.Node
	if useMaster {
		for _, n := range nodes.Items {
			if n.Labels["kubernetes.io/role"] == "master" {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/control-plane"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/controlplane"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/master"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node.kubernetes.io/master"]; exists {
				master = n
				break
			}
		}

		if master.Name == "" {
			return "", errors.New("Could not find any master node to run privileged pod on")
		}
	}

	// Create privileged pod. See https://miminar.fedorapeople.org/_preview/openshift-enterprise/registry-redeploy/go_client/executing_remote_processes.html.
	app := "hcc-test-" + strings.ToLower(base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(time.Now().Nanosecond())))[:5])

	var tolerations []apiv1.Toleration

	if useMaster {
		for _, taint := range master.Spec.Taints {
			tolerations = append(tolerations, apiv1.Toleration{
				Key:      taint.Key,
				Operator: apiv1.TolerationOpExists,
				Effect:   taint.Effect,
			})
		}
	}

	podConfig := &apiv1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name: app,
		},
		Spec: apiv1.PodSpec{
			Containers: []apiv1.Container{
				{
					Name:    app,
					Image:   "busybox",
					Command: []string{"cat"},
					Stdin:   true,
				},
			},
			HostPID:     true,
			NodeName:    master.Name,
			Tolerations: tolerations,
		},
	}

	// Mount /proc and /sys. Some popular CRs will mount both by default:
	// - https://github.com/containerd/containerd/blob/v1.6.4/oci/mounts.go#L29-L63
	// - https://github.com/moby/moby/blob/master/oci/defaults.go#L45-L69
	// But, others don't:
	// - https://github.com/cri-o/cri-o/blob/v1.24.0/server/container_create_linux.go#L507-L537 (conditionally)
	// So, we cannot rely on it and will have to take matters into own mounts
	podConfig.Spec.Volumes = []apiv1.Volume{
		apiv1.Volume{
			Name: "sysfs",
			VolumeSource: apiv1.VolumeSource{
				HostPath: &apiv1.HostPathVolumeSource{
					Path: "/sys",
				},
			},
		},
		// we could be able to control proc mount through spec.containers[].securityContext.procMount,
		// but on some k8s distro's there are (PS)policies preventing that. This works.
		apiv1.Volume{
			Name: "proc",
			VolumeSource: apiv1.VolumeSource{
				HostPath: &apiv1.HostPathVolumeSource{
					Path: "/proc",
				},
			},
		},
	}
	podConfig.Spec.Containers[0].VolumeMounts = []apiv1.VolumeMount{
		apiv1.VolumeMount{
			Name:      "sysfs",
			MountPath: "/sys",
			ReadOnly:  true,
		},
		apiv1.VolumeMount{
			Name:      "proc",
			MountPath: "/proc",
		},
	}

	pod, err := config.Kube.Pods().Create(config.Namespace, podConfig)
	if err != nil {
		return "", err
	}

	defer func() {
		if err := config.Kube.Pods().Delete(pod.Namespace, pod.Name); err != nil {
			logging.Error("Could not cleanup pod: '%s'", err.Error())
		}
	}()

	// Wait for 60sec for pod to become ready.
	ticker := time.NewTicker(config.GlobalInterval)
	timeout := time.After(60 * time.Second)
	var podHealthy bool

	for {
		select {
		case <-ticker.C:
			po, err := config.Kube.Pods().Get(config.Namespace, pod.Name)
			if err != nil {
				continue
			}

			if po.Status.Phase != apiv1.PodRunning {
				continue
			}

			podHealthy = true
			ticker.Stop()

		case <-timeout:
			logging.Error("timed out")
			ticker.Stop()
		}
		break
	}

	if !podHealthy {
		return "", fmt.Errorf("busybox pod has not become healthy after 60 seconds")
	}

	command := []string{"sh", "-c", cmd}

	output, err := config.Kube.Pods().ExecNamed(pod.Namespace, pod.Name, command)
	if err != nil {
		return "", err
	}

	if len(output) != 0 {
		output += "\n" + output
	}

	return output, nil
}
