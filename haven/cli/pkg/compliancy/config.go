// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package compliancy

import (
	"time"

	"github.com/Masterminds/semver/v3"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/haven"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/sonobuoy"
)

// Config holds the configuration of the checker
type Config struct {
	HavenReleases   HavenReleases
	KubeServer      *semver.Version
	KubeLatest      string
	Kube            kubernetes.KubeClient
	Sono            sonobuoy.Client
	CrdClient       haven.V1Alpha1
	Namespace       string
	CNCFConfigured  bool
	RunCISChecks    bool
	HostPlatform    Platform
	CisCheckAppName string
	GlobalInterval  time.Duration
}
