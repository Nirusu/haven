// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package compliancy

import (
	"errors"
	"fmt"

	"github.com/Masterminds/semver/v3"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	authorizationv1 "k8s.io/api/authorization/v1"
)

/*
	Fundamental checks in case of failure should immediately stop further
	processing as they are critical to properly checking for Haven Compliancy.
*/

func fundamentalHavenVersion(config *Config) (Result, error) {
	if Version == "undefined" {
		return ResultSkipped, nil
	}

	lv, err := semver.NewVersion(config.HavenReleases.latest().Version)
	if err != nil {
		return ResultNo, fmt.Errorf("Haven latest version formatting issue: %s, Error: %s", config.HavenReleases.latest().Version, err.Error())
	}

	hv, err := semver.NewVersion(Version)
	if err != nil {
		return ResultNo, fmt.Errorf("Haven current version formatting issue: %s, Error: %s", Version, err.Error())
	}

	LatestMajor, _ := semver.NewConstraint(fmt.Sprintf(">= %d.0.0", lv.Major()))

	if LatestMajor.Check(hv) {
		return ResultYes, nil
	}

	daysBetween := int(config.HavenReleases.latest().Date.Sub(config.HavenReleases.findByVersion(Version).Date).Hours() / 24)

	if daysBetween <= 90 {
		logging.Warning("You are currently using outdated Haven CLI version %s: the latest stable version %s has been released %d days ago", Version, config.HavenReleases.latest().Version, daysBetween)

		return ResultYes, nil
	}

	return ResultNo, fmt.Errorf("Your Haven CLI version %s is not the latest major version %s. Futhermore the 3 months upgrade window has expired. Please upgrade the Haven CLI and try again.", Version, config.HavenReleases.latest().Version)
}

func fundamentalClusterAdmin(config *Config) (Result, error) {
	sar := &authorizationv1.SelfSubjectAccessReview{
		Spec: authorizationv1.SelfSubjectAccessReviewSpec{
			NonResourceAttributes: &authorizationv1.NonResourceAttributes{
				Verb: "*",
				Path: "*",
			},
		},
	}

	response, err := config.Kube.AccessReview().Create(sar)

	if err != nil {
		return ResultNo, err
	}

	if !response.Status.Allowed {
		return ResultNo, errors.New("Haven Compliancy Checker requires cluster-admin access.")
	}

	return ResultYes, nil
}
